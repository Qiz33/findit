declare module gl {
    var LINEAR: number;
    var NEAREST: number;
    var REPEAT: number;
}
declare module cc {
    
    var SPRITE_INDEX_NOT_INITIALIZED:any;
    var TMX_ORIENTATION_HEX:any;
    var TMX_ORIENTATION_ISO:any;
    var TMX_ORIENTATION_ORTHO: any;

    var Z_COMPRESSION_BZIP2:any;
    var Z_COMPRESSION_GZIP:any;
    var Z_COMPRESSION_NONE:any;
    var Z_COMPRESSION_ZLIB: any;

    var BLEND_DST:any;
    var BLEND_SRC: any;

    var DIRECTOR_IOS_USE_BACKGROUND_THREAD:any;
    var DIRECTOR_MAC_THREAD:any;
    var DIRECTOR_STATS_INTERVAL:any;
    var ENABLE_BOX2_D_INTEGRATION:any;
    var ENABLE_DEPRECATED:any;
    var ENABLE_GL_STATE_CACHE:any;
    var ENABLE_PROFILERS:any;
    var ENABLE_STACKABLE_ACTIONS:any;
    var FIX_ARTIFACTS_BY_STRECHING_TEXEL:any;
    var GL_ALL:any;
    var LABELATLAS_DEBUG_DRAW:any;
    var LABELBMFONT_DEBUG_DRAW:any;
    var MAC_USE_DISPLAY_LINK_THREAD:any;
    var MAC_USE_MAIN_THREAD:any;
    var MAC_USE_OWN_THREAD:any;
    var NODE_RENDER_SUBPIXEL:any;
    var PVRMIPMAP_MAX:any;
    var SPRITEBATCHNODE_RENDER_SUBPIXEL:any;
    var SPRITE_DEBUG_DRAW:any;
    var TEXTURE_ATLAS_USE_TRIANGLE_STRIP:any;
    var TEXTURE_ATLAS_USE_VAO:any;
    var USE_L_A88_LABELS:any;
    var ACTION_TAG_INVALID:any;
    var DEVICE_MAC:any;
    var DEVICE_MAC_RETINA_DISPLAY:any;
    var DEVICEI_PAD:any;
    var DEVICEI_PAD_RETINA_DISPLAY:any;
    var DEVICEI_PHONE:any;
    var DEVICEI_PHONE5:any;
    var DEVICEI_PHONE5_RETINA_DISPLAY:any;
    var DEVICEI_PHONE_RETINA_DISPLAY:any;
    var DIRECTOR_PROJECTION2_D:any;
    var DIRECTOR_PROJECTION3_D:any;
    var DIRECTOR_PROJECTION_CUSTOM:any;
    var DIRECTOR_PROJECTION_DEFAULT:any;
    var FILE_UTILS_SEARCH_DIRECTORY_MODE:any;
    var FILE_UTILS_SEARCH_SUFFIX_MODE:any;
    var FLIPED_ALL:any;
    var FLIPPED_MASK:any;
    var IMAGE_FORMAT_JPEG:any;
    var IMAGE_FORMAT_PNG:any;
    var ITEM_SIZE:any;
    var LABEL_AUTOMATIC_WIDTH:any;
    var LINE_BREAK_MODE_CHARACTER_WRAP:any;
    var LINE_BREAK_MODE_CLIP:any;
    var LINE_BREAK_MODE_HEAD_TRUNCATION:any;
    var LINE_BREAK_MODE_MIDDLE_TRUNCATION:any;
    var LINE_BREAK_MODE_TAIL_TRUNCATION:any;
    var LINE_BREAK_MODE_WORD_WRAP:any;
    var MAC_VERSION_10_6:any;
    var MAC_VERSION_10_7:any;
    var MAC_VERSION_10_8:any;
    var MENU_HANDLER_PRIORITY:number;
    var MENU_STATE_TRACKING_TOUCH:number;
    var MENU_STATE_WAITING:number;
    var NODE_TAG_INVALID:any;
    var PARTICLE_DURATION_INFINITY:any;
    var PARTICLE_MODE_GRAVITY:any;
    var PARTICLE_MODE_RADIUS:any;
    var PARTICLE_START_RADIUS_EQUAL_TO_END_RADIUS:any;
    var PARTICLE_START_SIZE_EQUAL_TO_END_SIZE:any;
    var POSITION_TYPE_FREE:any;
    var POSITION_TYPE_GROUPED:any;
    var POSITION_TYPE_RELATIVE:any;
    var PRIORITY_NON_SYSTEM_MIN:any;
    var PRIORITY_SYSTEM: any;

    var PROGRESS_TIMER_TYPE_BAR:number;
    var PROGRESS_TIMER_TYPE_RADIAL: number;

    var REPEAT_FOREVER: number;
    var RESOLUTION_MAC: number;

    var RESOLUTION_MAC_RETINA_DISPLAY: number;
    var RESOLUTION_UNKNOWN: number;
    var TMX_TILE_DIAGONAL_FLAG: number;
    var TMX_TILE_HORIZONTAL_FLAG: number;
    var TMX_TILE_VERTICAL_FLAG: number;
    var TEXT_ALIGNMENT_CENTER:number;
    var TEXT_ALIGNMENT_LEFT:number;
    var TEXT_ALIGNMENT_RIGHT: number;

    var TEXTURE2_D_PIXEL_FORMAT_A8:any;
    var TEXTURE2_D_PIXEL_FORMAT_A_I88:any;
    var TEXTURE2_D_PIXEL_FORMAT_DEFAULT:any;
    var TEXTURE2_D_PIXEL_FORMAT_I8:any;
    var TEXTURE2_D_PIXEL_FORMAT_PVRTC2:any;
    var TEXTURE2_D_PIXEL_FORMAT_PVRTC4:any;
    var TEXTURE2_D_PIXEL_FORMAT_RG_B565:any;
    var TEXTURE2_D_PIXEL_FORMAT_RGB5_A1:any;
    var TEXTURE2_D_PIXEL_FORMAT_RG_B888:any;
    var TEXTURE2_D_PIXEL_FORMAT_RGB_A4444:any;
    var TEXTURE2_D_PIXEL_FORMAT_RGB_A8888:any;
    var TOUCHES_ALL_AT_ONCE:any;
    var TOUCHES_ONE_BY_ONE:any;
    var TRANSITION_ORIENTATION_DOWN_OVER:any;
    var TRANSITION_ORIENTATION_LEFT_OVER:any;
    var TRANSITION_ORIENTATION_RIGHT_OVER:any;
    var TRANSITION_ORIENTATION_UP_OVER:any;
    var UNIFORM_COS_TIME:any;
    var UNIFORM_MV_MATRIX:any;
    var UNIFORM_MVP_MATRIX:any;
    var UNIFORM_P_MATRIX:any;
    var UNIFORM_RANDOM01:any;
    var UNIFORM_SAMPLER:any;
    var UNIFORM_SIN_TIME:any;
    var UNIFORM_TIME:any;
    var UNIFORM_MAX:any;
    var VERTEX_ATTRIB_FLAG_COLOR:any;
    var VERTEX_ATTRIB_FLAG_NONE:any;
    var VERTEX_ATTRIB_FLAG_POS_COLOR_TEX:any;
    var VERTEX_ATTRIB_FLAG_POSITION:any;
    var VERTEX_ATTRIB_FLAG_TEX_COORDS:any;
    var VERTEX_ATTRIB_COLOR:any;
    var VERTEX_ATTRIB_MAX:any;
    var VERTEX_ATTRIB_POSITION:any;
    var VERTEX_ATTRIB_TEX_COORDS: any;

    var VERTICAL_TEXT_ALIGNMENT_BOTTOM:number;
    var VERTICAL_TEXT_ALIGNMENT_CENTER:number;
    var VERTICAL_TEXT_ALIGNMENT_TOP: number;

    var OS_VERSION_4_0:any;
    var OS_VERSION_4_0_1:any;
    var OS_VERSION_4_1:any;
    var OS_VERSION_4_2:any;
    var OS_VERSION_4_2_1:any;
    var OS_VERSION_4_3:any;
    var OS_VERSION_4_3_1:any;
    var OS_VERSION_4_3_2:any;
    var OS_VERSION_4_3_3:any;
    var OS_VERSION_4_3_4:any;
    var OS_VERSION_4_3_5:any;
    var OS_VERSION_5_0:any;
    var OS_VERSION_5_0_1:any;
    var OS_VERSION_5_1_0:any;
    var OS_VERSION_6_0_0:any;
    var ANIMATION_FRAME_DISPLAYED_NOTIFICATION:any;
    var CHIPMUNK_IMPORT:any;
    var ATTRIBUTE_NAME_COLOR:any;
    var ATTRIBUTE_NAME_POSITION:any;
    var ATTRIBUTE_NAME_TEX_COORD:any;
    var SHADER_POSITION_COLOR:any;
    var SHADER_POSITION_LENGTH_TEXURE_COLOR:any;
    var SHADER_POSITION_TEXTURE:any;
    var SHADER_POSITION_TEXTURE_A8_COLOR:any;
    var SHADER_POSITION_TEXTURE_COLOR:any;
    var SHADER_POSITION_TEXTURE_COLOR_ALPHA_TEST:any;
    var SHADER_POSITION_TEXTURE_U_COLOR:any;
    var SHADER_POSITION_U_COLOR:any;
    var UNIFORM_ALPHA_TEST_VALUE_S:any;
    var UNIFORM_COS_TIME_S:any;
    var UNIFORM_MV_MATRIX_S:any;
    var UNIFORM_MVP_MATRIX_S:any;
    var UNIFORM_P_MATRIX_S:any;
    var UNIFORM_RANDOM01_S:any;
    var UNIFORM_SAMPLER_S:any;
    var UNIFORM_SIN_TIME_S:any;
    var UNIFORM_TIME_S:any;

    var LANGUAGE_ENGLISH:any;
    var LANGUAGE_CHINESE:any;
    var LANGUAGE_FRENCH:any;
    var LANGUAGE_ITALIAN:any;
    var LANGUAGE_GERMAN:any;
    var LANGUAGE_SPANISH:any;
    var LANGUAGE_RUSSIAN:any;
    var LANGUAGE_KOREAN:any;
    var LANGUAGE_JAPANESE:any;
    var LANGUAGE_HUNGARIAN:any;
    var LANGUAGE_PORTUGUESE:any;
    var LANGUAGE_ARABIC:any;
    
    var DIRECTOR_PROJECTION_2D:any;
    var DIRECTOR_PROJECTION_3D:any;

    var TEXTURE_PIXELFORMAT_RGBA8888:any;
    var TEXTURE_PIXELFORMAT_RGB888:any;
    var TEXTURE_PIXELFORMAT_RGB565:any;
    var TEXTURE_PIXELFORMAT_A8:any;
    var TEXTURE_PIXELFORMAT_I8:any;
    var TEXTURE_PIXELFORMAT_AI88:any;
    var TEXTURE_PIXELFORMAT_RGBA4444:any;
    var TEXTURE_PIXELFORMAT_RGB5A1:any;
    var TEXTURE_PIXELFORMAT_PVRTC4:any;
    var TEXTURE_PIXELFORMAT_PVRTC4:any;
    var TEXTURE_PIXELFORMAT_DEFAULT:any;

    var TEXT_ALIGNMENT_LEFT:number;
    var TEXT_ALIGNMENT_CENTER: number;
    var TEXT_ALIGNMENT_RIGHT: number;

    var VERTICAL_TEXT_ALIGNMENT_TOP: number;
    var VERTICAL_TEXT_ALIGNMENT_CENTER: number;
    var VERTICAL_TEXT_ALIGNMENT_BOTTOM: number;

    var IMAGE_FORMAT_JPEG:any;
    var IMAGE_FORMAT_PNG:any;

    var PROGRESS_TIMER_TYPE_RADIAL: number;
    var PROGRESS_TIMER_TYPE_BAR: number;

    var PARTICLE_TYPE_FREE:any;
    var PARTICLE_TYPE_RELATIVE:any;
    var PARTICLE_TYPE_GROUPED:any;
    var PARTICLE_DURATION_INFINITY:any;
    var PARTICLE_MODE_GRAVITY:any;
    var PARTICLE_MODE_RADIUS:any;
    var PARTICLE_START_SIZE_EQUAL_TO_END_SIZE:any;
    var PARTICLE_START_RADIUS_EQUAL_TO_END_RADIUS:any;

    var TOUCH_ALL_AT_ONCE:any;
    var TOUCH_ONE_BY_ONE:any;

    var TRANSITION_ORIENTATION_LEFT_OVER:any;
    var TRANSITION_ORIENTATION_RIGHT_OVER:any;
    var TRANSITION_ORIENTATION_UP_OVER:any;
    var TRANSITION_ORIENTATION_DOWN_OVER:any;

    var RED:any;
    var GREEN:any;
    var BLUE:any;
    var BLACK:any;
    var WHITE:any;
    var YELLOW:any;

    var POINT_ZERO:any;

    // XXX: This definition is different than cocos2d-html5
    // var REPEAT_FOREVER 
    // We can't assign -1 to var REPEAT_FOREVER, since it will be a very big double value after
    // converting it to double by JS_ValueToNumber on android.
    // Then cast it to unsigned int, the value will be 0. The schedule will not be able to work.
    // I don't know why this ovar rs only on android.
    // So instead of passing -1 to it, I assign it with max value of unsigned int in c++.
    var REPEAT_FOREVER: number;

    var MENU_STATE_WAITING: number;
    var MENU_STATE_TRACKING_TOUCH: number;
    var MENU_HANDLER_PRIORITY: number;
    var DEFAULT_PADDING: number;

    var SCROLLVIEW_DIRECTION_NONE: number;
    var SCROLLVIEW_DIRECTION_HORIZONTAL: number;
    var SCROLLVIEW_DIRECTION_VERTICAL: number;
    var SCROLLVIEW_DIRECTION_BOTH: number;
    var TABLEVIEW_FILL_TOPDOWN: number;
    var TABLEVIEW_FILL_BOTTOMUP: number;


    /**
     * @constant
     * @type Number
     */
    var KEYBOARD_RETURNTYPE_DEFAULT:any;

    /**
     * @constant
     * @type Number
     */
    var KEYBOARD_RETURNTYPE_DONE:any;

    /**
     * @constant
     * @type Number
     */
    var KEYBOARD_RETURNTYPE_SEND:any;

    /**
     * @constant
     * @type Number
     */
    var KEYBOARD_RETURNTYPE_SEARCH:any;

    /**
     * @constant
     * @type Number
     */
    var KEYBOARD_RETURNTYPE_GO:any;

    /**
     * The EditBoxInputMode defines the type of text that the user is allowed * to enter.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_MODE_ANY:any;

    /**
     * The user is allowed to enter an e-mail address.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_MODE_EMAILADDR:any;

    /**
     * The user is allowed to enter an integer value.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_MODE_NUMERIC:any;

    /**
     * The user is allowed to enter a phone number.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_MODE_PHONENUMBER:any;

    /**
     * The user is allowed to enter a URL.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_MODE_URL:any;

    /**
     * The user is allowed to enter a real number value.
     * This extends kEditBoxInputModeNumeric by allowing a decimal point.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_MODE_DECIMAL:any;

    /**
     * The user is allowed to enter any text, except for line breaks.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_MODE_SINGLELINE:any;

    /**
     * Indicates that the text entered is confidential data that should be
     * obscured whenever possible. This implies EDIT_BOX_INPUT_FLAG_SENSITIVE.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_FLAG_PASSWORD:any;

    /**
     * Indicates that the text entered is sensitive data that the
     * implementation must never store into a dictionary or table for use
     * in predictive, auto-completing, or other avar lerated input schemes.
     * A credit card number is an example of sensitive data.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_FLAG_SENSITIVE:any;

    /**
     * This flag is a hint to the implementation that during text editing,
     * the initial letter of each word should be capitalized.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_FLAG_INITIAL_CAPS_WORD:any;

    /**
     * This flag is a hint to the implementation that during text editing,
     * the initial letter of each sentence should be capitalized.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_FLAG_INITIAL_CAPS_SENTENCE:any;

    /**
     * Capitalize all characters automatically.
     * @constant
     * @type Number
     */
    var EDITBOX_INPUT_FLAG_INITIAL_CAPS_ALL_CHARACTERS:any;

    var log: any;
    //Color 3B
    export class Color3B {
        r: number;
        g: number;
        b: number;
        constructor(r: number, g: number, b:number);
    }
    var c3b: (r: number, g: number, b: number)=>Color3B;
    //Color 4B
    export class Color4B {
        r: number;
        g: number;
        b: number;
        a: number;
        constructor(r: number, g: number, b: number, a:number);
    }
    var c4b: (r: number, g: number, b: number, a: number) => Color4B;

    //Color 4F
    export class Color4F {
        r: number;
        g: number;
        b: number;
        a: number;
        constructor(r: number, g: number, b: number, a: number);
    }
    var c4f: (r: number, g: number, b: number, a: number) => Color4F;
    
    //Point, ccp
    export class Point {
        x: number;
        y: number;
        constructor(x:number, y:number);
    }
    var p: (x: number, y: number) => Point;
    var pointEqualToPoint: (point1: Point, point2: Point) => boolean;

    export class Size {
        width: number;
        height: number;
        constructor(w: number, h: number);
    }
    var size: (w: number, h: number) => Size;
    var sizeEqualToSize: (size1: Size, size2: Size) => boolean;

    export class Rect {
        x: number;
        y: number;
        width: number;
        height: number;
        constructor(x: number, y: number, w: number, h: number);
    }
    var rect: (x: number, y: number, w: number, h: number) => Rect;

    var rectEqualToRect: (rect1: Rect, rect2: Rect) => boolean;
    var rectContainsRect: (rect1: Rect, rect2: Rect) => boolean;
    var rectGetMaxX: (rect: Rect) => number;
    var rectGetMidX: (rect: Rect) => number;
    var rectGetMinX: (rect: Rect) => number;
    var rectGetMaxY: (rect: Rect) => number;
    var rectGetMidY: (rect: Rect) => number;
    var rectGetMinY: (rect: Rect) => number;
    var rectContainsPoint: (rect: Rect, point: Point) => boolean;
    var rectIntersectsRect: (rectA: Rect, rectB: Rect) => boolean;
    var rectUnion: (rectA: Rect, rectB: Rect) => Rect;
    var rectIntersection: (rectA: Rect, rectB: Rect) => Rect;

    var ArrayGetIndexOfObject: (arr: Array<any>, findObj: any) => number;
    var ArrayContainsObject: (arr: Array<any>, findObj: any) => boolean;
    var ArrayRemoveObject: (arr: Array<any>, delObj: any) => void;

    var dump: (obj: any) => void;
    var dumpConfig: () => void;

    var associateWithNative: (jsobj: any, superclass_or_instanc: any) => void;
    var inherits: (childCtor: any, parentCtor: any) => void;
    var base: (me:any, opt_methodName:any, var_args:any) => void;

    export class Class {
        _super(): void;
    }
    export class GLProgram extends Class{
        addAttribute(attributeName: string, index: number): void;
        link(): boolean;
        use(): void;
        updateUniforms(): void;

        static create(vShaderFileName:string, fShaderFileName:string): GLProgram;
    }
    export class Node extends Class{
        cleanup(): void;
        
        visit(): void;
        transform(): void;

        getScriptHandler(): any;
        unregisterScriptHandler(): void;
        
        setShaderProgram(newShaderProgram: GLProgram): void;
        getShaderProgram(): GLProgram;

        convertToNodeSpace(p: Point): Point;//Point
        convertToWorldSpace(p: Point): Point;//Point
        convertToNodeSpaceAR(p: Point): Point;//Point
        convertToWorldSpaceAR(p: Point): Point;//Point

        convertTouchToNodeSpace(p: Touch): Point;//Point
        convertTouchToNodeSpaceAR(p: Touch): Point;//Point

        isIgnoreAnchorPointForPosition(): boolean;
        ignoreAnchorPointForPosition(v: boolean);

        getGrid(): GridBase;
        setGrid(grid: GridBase): void;

        setParent(v: Node): void;
        getParent(): Node;

        getRotation(): number;
        setRotation(v: number): void;

        getRotationX(): number;
        setRotationX(v: number): void;

        getRotationY(): number;
        setRotationY(v: number): void;

        getScale(): number;
        setScale(v: number): void;

        getScaleY(): number;
        setScaleY(v: number): void;

        getScaleX(): number;
        setScaleX(v: number): void;

        getSkewX(): number;
        setSkewX(v: number): void;

        setSkewY(v: number): void;
        getSkewY(): number;

        getVertexZ(): number;
        setVertexZ(v: number): void;

        getZOrder(): number;
        setZOrder(v: number): void;
        
        getTag(): number;
        setTag(v: number): void;

        setPosition(x:number, y:number): void;
        getPosition(): Point;

        getPositionX(): number;
        setPositionX(v: number): void;

        getPositionY(): number;
        setPositionY(v: number): void;

        getOrderOfArrival(): number;
        setOrderOfArrival(v: number): void;

        isVisible(): boolean;
        setVisible(v: boolean): void;

        getAnchorPoint(): Point;
        getAnchorPointInPoints(): Point;
        setAnchorPoint(p: Point): void;

        getBoundingBox(): Rect;

        setContentSize(s: Size): void;
        getContentSize(): Size;

        getActionManager(): any;
        getScheduler(): any;

        isRunning(): boolean;

        getChildrenCount(): number;
        getChildren(): any[];
        
        getChildByTag(tag: number): Node;
        addChild(child:Node, zOrder?:number, tag?:number): void;
        removeFromParent(cleanup?: boolean): void;
        removeFromParentAndCleanup(cleanup?: boolean): void;
        removeChild(child: Node, cleanup?: boolean): void;

        removeChildByTag(tag: number, cleanup?: boolean): void;
        removeAllChildrenWithCleanup(cleanup?: boolean): void;
        removeAllChildren(cleanup?: boolean): void;

        reorderChild(child: Node, zOrder: number): void;
        sortAllChildren(): void;

        draw(ctx: any): void;

        onEnter(): void;
        onEnterTransitionDidFinish(): void;
        onExitTransitionDidStart(): void;
        onExit(): void;
        
        runAction(action: Action): void;
        stopAction(action: Action): void;
        stopAllActions(): void;
        stopActionByTag(tag: number): void;
        getActionByTag(tag: number): Action;
        numberOfRunningActions(): number;

        static create(...args: any[]): Node;
        static extend(prop: any): Node;
    }
    export class NodeRGBA extends Node {
        getOpacity(): number;
        setOpacity(opacity: number): void;

        getDisplayedOpacity(): number;
        updateDisplayedOpacity(v: number): void;

        getColor(): Color3B;
        setColor(color3: Color3B): void;

        getDisplayedColor(): Color3B;
        updateDisplayedColor(v: Color3B): void;

        isCascadeOpacityEnabled(): boolean;
        setCascadeOpacityEnabled(v: boolean): void;

        isCascadeColorEnabled(): boolean;
        setCascadeColorEnabled(v: boolean): void;

        isOpacityModifyRGB(): boolean;
        setOpacityModifyRGB(v: boolean): void;
    }
    export class ClippingNode extends Node {
        getStencil(): Node;
        setStencil(stencil: Node): void;

        isInverted(): boolean;
        setInverted(v: boolean): void;

        static create(stencil?: Node): ClippingNode;
    }
    export class GridBase {
        getGridSize(): Size;
        setGridSize(v: Size): void;
    }

    export class Sprite extends NodeRGBA {
        getTexture(): Texture2D;
        setTexture(v: Texture2D): void;
        
        setDisplayFrame(frame: SpriteFrame): void;
        getTextureRect(): Rect;
        getOffsetPosition(): Point;
        
        static createWithSpriteFrameName(spriteFrameName:string): Sprite;
        static createWithSpriteFrame(spriteFrame: SpriteFrame): Sprite;
        static createWithTexture(texture:Texture2D, rect?:Rect, offset?:Point): Sprite;
        static create(...args: any[]): Sprite;
    }
    var _texParams: (minFilter:number, magFilter:number, wrapS:number, wrapT:number) => void;
        
    export class Texture2D extends Class {
        getName(): string;
        setTexParameters(texParams: any): void;
    }
    export class SpriteFrame extends Class{
        getTexture(): Texture2D;
        setTexture(v: Texture2D): void;
        static create(filename: string, rect: Rect, rotated: boolean, offset?: Point, originalSize?: Size): SpriteFrame;
        static createWithTexture(texture: Texture2D, rect: Rect, rotated: boolean, offset?: Point, originalSize?: Size): SpriteFrame; 
    }

    export class SpriteFrameCache {
        static getInstance(): SpriteFrameCache;

        addSpriteFrames(plist: string, texture: string): void;
        addSpriteFrame(frame: SpriteFrame, frameName: string): void;

        getSpriteFrame(name: string): SpriteFrame;

        removeSpriteFrames(): void;
        removeSpriteFrameByName(name: string): void;
        removeSpriteFramesFromFile(plist: string): void;
    }

    export class TextureCache {
        dumpCachedTextureInfo(): string;
        removeAllTextures(): void;
        removeTexture(texture: Texture2D): void;
        removeTextureForKey(textureKeyName: string): void;
        addImage(image: string): Texture2D;

        textureForKey(textureKeyName: string): Texture2D;

        static getInstance(): TextureCache;
    }

    export class AnimationFrame extends Class {
        getSpriteFrame(): SpriteFrame;
        setSpriteFrame(v: SpriteFrame): void
    }
    export class Animation extends Class {
        addSpriteFrame(v: SpriteFrame): void;
        addSpriteFrameWithFile(v: string): void;
        addSpriteFrameWithTexture(texture: Texture2D, rect?: Rect): void;
        
        getLoops(): number;
        setLoops(v: number): void;

        getDuration(): number;
        setDuration(v: number): void;

        getDelayPerUnit(): number;
        setDelayPerUnit(v: number): void;

        getTotalDelayUnits(): number;
    }
    export class LabelTTF extends Sprite{
        getString(): string;
        setString(text: string): void;

        getDuration(): number;
        setDuration(v: number): void;

        getHorizontalAlignment(): number;
        setHorizontalAlignment(v: number): void;

        getVerticalAlignment(): number;
        setVerticalAlignment(v: number): void;

        getDimensions(): Size;
        setDimensions(v: Size): void;

        getFontSize(): number;
        getFontSize(v: number): void;

        getFontName(): string;
        setFontName(v: string): void;

        enableShadow(shadowOffset: Size, shadowOpacity: number, shadowBlur: number, mustUpdateTexture?: boolean): void;
        disableShadow(mustUpdateTexture?: boolean): void;

        enableStroke(strokeColor: Color3B, strokeSize: number, mustUpdateTexture?: boolean): void;
        disableStroke(mustUpdateTexture?: boolean): void;

        setFontFillColor(tintColor: Color3B, mustUpdateTexture?: boolean): void;

        static create(label:string, fontName?:String, fontSize?:number, dimensions?: Size, alignment?:number): LabelTTF;
    }
    export class SpriteBatchNode extends Node {

    }
    export class LabelBMFont extends SpriteBatchNode {
        getOpacity(): number;
        setOpacity(opacity: number): void;

        getColor(): Color3B;
        setColor(color3: Color3B): void;

        createFontChars(): void;
        updateLabel(): void;

        getString(): string;
        setString(text: string): void;

        getFntFile(): string;
        setFntFile(v: string): void;

        setLineBreakWithoutSpace(breakWithoutSpace: boolean): void;
        setAlignment(v: number): void;
        setWidth(v: number): void;

        purgeCachedData(): void;

        static create(str:string, fntFile:string, width?:number, alignment?:number, imageOffset?: Size): LabelBMFont;
    }
    export class Touch {
        getPreviousLocationInView(): Point;
        getLocation(): Point;
        getDelta(): Point;
        getStartLocationInView(): Point;
        getStartLocation(): Point;
        getID(): number;
        getLocationInView(): Point;
        getPreviousLocation(): Point;
        setTouchInfo(id:number, x:number, y:number): void;
    }
    export class Set {
        count(): number;
        addObject(obj: any): void;
        removeAllObjects(): void;
        removeObject(obj: any): void;
        containsObject(obj: any): boolean;
        anyObject(): any;
        static create(): Set;
    }
    export class Layer extends Node {
        setTouchEnabled(t: boolean): void;
        isTouchEnabled(): boolean;

        setAccelerometerEnabled(t: boolean): void;
        isAccelerometerEnabled(): boolean;

        setKeypadEnabled(t: boolean): void;
        isKeypadEnabled(): boolean;

        getTouchPriority(): number;
        setTouchPriority(v: number): void;

        getTouchMode(): number;
        setTouchMode(v: number): void;

        keyBackClicked(): void;

        onTouchBegan(touch: Touch, event: any): boolean;
        onTouchMoved(touch: Touch, event: any): void;
        onTouchEnded(touch: Touch, event: any): void;
        onTouchCancelled(touch: Touch, event: any): void;

        static create(...args:any[]): Layer;
        static extend(prop: any): Layer;
    }

    export class LayerColor extends Layer {
        changeWidthAndHeight(w: number, h: number): void;
        changeWidth(w: number): void;
        changeHeight(h: number): void;

        getOpacity(): number;
        setOpacity(v: number): void;

        getColor(): Color3B;
        setColor(v: Color3B): void;

        static create(color?:Color4B, width?: number, height?: number): LayerColor;
        static extend(prop: any): LayerColor;
    }

    export class Scene extends Node{
        static create(): Scene;
        static extend(prop: any): Scene;
    }
    export class Scheduler {
        scheduleCallbackForTarget(target: any, callback_fn: (dt?: number) => void, interval?: number, repeat?: number, delay?: number, paused?: boolean): void;
        scheduleUpdateForTarget(target: any, priority: number, paused: boolean): void;
        unscheduleCallbackForTarget(target: any, callback_fn: (dt?: number) => void): void;
        unscheduleUpdateForTarget(target: any): void;
        unscheduleAllCallbacksForTarget(target: any): void;
        unscheduleAllCallbacks(): void;
        unscheduleAllCallbacksWithMinPriority(minPriority: number): void;
    }
    export class Director {
        purgeCachedData(): void;
        end(): void;
        pause(): void;
        resume(): void;

        isPaused(): boolean;

        popScene(): void;
        pushScene(scene: Scene): void;
        replaceScene(scene: Scene): void;
        runWithScene(scene: Scene): void;
        popToRootScene(): void;

        getRunningScene(): Scene;

        setContentScaleFactor(t: number): void;
        getContentScaleFactor(): number;

        setDisplayStats(t: boolean): void;
        isDisplayStats(): boolean;

        getVisibleSize(): number;
        getWinSize(): Size;
        
        getScheduler(): Scheduler;

        setDepthTest(on: boolean): void;

        static getInstance(): Director;
    }
    export class Application extends Class{
        getTargetPlatform(): string;
        getCurrentLanguage(): string;
        static sharedApplication(): Application;
    }
    export class Action extends Class{
        startWithTarget(t: Node): void;
        setOriginalTarget(t: Node): void;
        setTarget(t: Node): void;
        getTarget(): Node;
        getOriginalTarget(): Node;

        stop(): void;
        update(t: number): void;
        step(t: number): void;

        setTag(t: number): void;
        getTag(): number;

        isDone(): boolean;
    }
    export class FiniteTimeAction extends Action {
    }
    export class ActionInterval extends FiniteTimeAction {
        setAmplitudeRate(t: number): void;
        getAmplitudeRate(): number;
        reverse(): ActionInterval;
    }
    export class Sequence extends ActionInterval {
        static create(...args:any[]): Sequence;
    }
    export class Spawn extends ActionInterval {
        static create(...args: any[]): Sequence;
    }
    export class Repeat extends ActionInterval {
        static create(action: FiniteTimeAction, times:number): Repeat;
    }
    export class RepeatForever extends ActionInterval {
        static create(action: FiniteTimeAction): RepeatForever;
    }
    export class RotateTo extends ActionInterval {
        static create(duration:number, deltaAngleX:number, deltaAngleY?:number): RotateTo;
    }
    export class RotateBy extends ActionInterval {
        static create(duration: number, deltaAngleX: number, deltaAngleY?: number): RotateBy;
    }
    export class MoveTo extends ActionInterval {
        static create(duration: number, position: Point): MoveTo;
    }
    export class MoveBy extends ActionInterval {
        static create(duration: number, position: Point): MoveBy;
    }
    export class SkewTo extends ActionInterval {
        static create(duration: number, sx: number, sy: number): SkewTo;
    }
    export class SkewBy extends ActionInterval {
        static create(duration: number, sx: number, sy: number): SkewBy;
    }
    export class ScaleTo extends ActionInterval {
        static create(duration: number, sx: number, sy: number): ScaleTo;
    }
    export class ScaleBy extends ActionInterval {
        static create(duration: number, sx: number, sy: number): ScaleBy;
    }
    export class Blink extends ActionInterval {
        static create(duration: number, blinks: number): Blink;
    }
    export class FadeIn extends ActionInterval {
        static create(duration: number): Blink;
    }
    export class FadeOut extends ActionInterval {
        static create(duration: number): FadeOut;
    }
    export class FadeTo extends ActionInterval {
        static create(duration: number, opacity: number): FadeTo;
    }
    export class TintTo extends ActionInterval {
        static create(duration: number, red:number, green:number, blue:number): TintTo;
    }
    export class TintBy extends ActionInterval {
        static create(duration: number, red: number, green: number, blue: number): TintBy;
    }

    export class JumpBy extends ActionInterval {
        static create(duration:number, position: Point, height:number, jumps:number): JumpBy;
    }
    export class JumpTo extends ActionInterval {
        static create(duration: number, position: Point, height: number, jumps: number): JumpTo;
    }
    export class BezierBy extends ActionInterval {
        static create(duration: number, c:any): BezierBy;
    }
    export class BezierTo extends ActionInterval {
        static create(duration: number, c: any): BezierTo;
    }
    export class DelayTime extends ActionInterval {
        static create(duration: number): DelayTime;
    }
    export class Animate extends ActionInterval {
        setAnimation(v: Animation): void;
        getAnimation(): Animation;
        static create(animation: Animation): Animate;
    }
    export class TargetedAction extends ActionInterval {
        setForcedTarget(v: Node):void;
        getForcedTarget(): Node;
        static create(target: Node, action: FiniteTimeAction): TargetedAction;
    }
    export class Speed extends Action {
        setSpeed(v: number): void;
        getSpeed(): number;
        static create(action: ActionInterval, speed: number): Speed;
    }
    export class Follow extends Action {
        setBoudarySet(v: boolean): void;
        isBoundarySet(): boolean;
        static create(followedNode: Node, rect: Rect): Follow;
    }
    export class ActionInstant extends FiniteTimeAction {
    }
    
    export class Show extends ActionInstant {
        static create(): Show;
    }
    export class Hide extends ActionInstant {
        static create(): Hide;
    }
    export class ToggleVisibility extends ActionInstant {
        static create(): ToggleVisibility;
    }
    export class FlipX extends ActionInstant {
        static create(x:boolean): FlipX;
    }
    export class FlipY extends ActionInstant {
        static create(x: boolean): FlipY;
    }
    export class Place extends ActionInstant {
        static create(pos: Point): Place;
    }
    export class CallFunc extends ActionInstant {
        static create(selector:(actionTarget?:Node, value?:any)=>void, selectorTarget?:Node, data?:any): Place;
    }
    export class ActionEase extends ActionInterval {
    }
    
    export class EaseIn extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseInOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }

    export class EaseExponentialIn extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseExponentialOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseExponentialInOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }

    export class EaseSineIn extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseSineOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseSineInOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }

    export class EaseElastic extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseElasticOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseElasticInOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }

    export class EaseBounceIn extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseBounceOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseBounceInOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    
    export class EaseBackIn extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseBackOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    export class EaseBackInOut extends ActionEase {
        static create(action: ActionInterval): ActionEase;
    }
    //var to = cc.ProgressTo.create(2, 100);
    export class ProgressTo extends ActionInterval {
        static create(duration:number, percent:number): ProgressTo;
    }
    export class ProgressFromTo extends ActionInterval {
        static create(duration: number, fromPercentage: number, toPercentage: number): ProgressFromTo;
    }
    export class ActionTween extends ActionInterval {
        static create(duration:number, key:string, from:number, to:number): ActionTween;
    }
    export class PageTurn3D extends ActionInterval {
        static create(duration: number, gridSize: Size): PageTurn3D;
    }
    export class FlipX3D extends ActionInterval {
        static create(duration: number): FlipX3D;
    }
    export class FlipY3D extends ActionInterval {
        static create(duration: number): FlipY3D;
    }
    export class Lens3D extends ActionInterval {
        static create(duration:number, gridSize: Size, position: Point, radius:number): FlipY3D;
    }
    //var action1 = cc.CardinalSplineTo.create(3, array, 0);points array of control points
    export class CardinalSplineTo extends ActionInterval {
        static create(duration: number, points: Point[], tension: number): CardinalSplineTo;
    }
    export class CardinalSplineBy extends ActionInterval {
        static create(duration: number, points: Point[], tension: number): CardinalSplineBy;
    }
    export class CatmullRomTo extends ActionInterval {
        static create(dt:number, points: Point[]): CatmullRomTo
    }
    export class CatmullRomBy extends ActionInterval {
        static create(dt: number, points: Point[]): CatmullRomTo
    }
    export class OrbitCamera extends ActionInterval {
        static create(t: number, radius: number, deltaRadius: number, angleZ: number, deltaAngleZ: number, angleX: number, deltaAngleX: number): OrbitCamera;
    }
    export class Menu extends Layer{
        isEnabled(): boolean;
        setEnabled(v: boolean): void;

        getOpacity(): number;
        setOpacity(v: number): void;

        getColor(): Color3B;
        setColor(v: Color3B): void;

        alignItemsVertically(): void;
        alignItemsHorizontally(): void;
        alignItemsVerticallyWithPadding(v: number): void;
        alignItemsHorizontallyWithPadding(v: number): void;

        static create(...args:any[]): Menu;
    }
    export class MenuItem extends Node {
        isSelected(): boolean;
        activate(): void;

        selected(): void;
        unselected(): void;

        isEnabled(): boolean;
        setEnabled(v: boolean): void;

        rect(): Rect;
    }
    export class MenuItemLabel extends MenuItem {
        getLabel(): Node;
        setLabel(v: Node): void;

        getOpacity(): number;
        setOpacity(v: number): void;

        getColor(): Color3B;
        setColor(v: Color3B): void;

        getDisabledColor(): Color3B;
        setDisabledColor(v: Color3B): void;

        static create(label: Node, selector?: (sender:Node) => void , target?: Node):MenuItemLabel;
    }
    export class MenuItemFont extends MenuItem {
        setFontSize(s: number): void;
        fontSize(): number;

        fontName(): string;
        setFontName(v: string): void;
        static create(str: string, selector?: (sender: Node) => void , target?: Node): MenuItemFont;
    }
    export class MenuItemSprite extends MenuItem {
        getNormalImage(): Node;
        setNormalImage(v: Node): void;

        getSelectedImage(): Node;
        setSelectedImage(v: Node): void;

        getDisabledImage(): Node;
        setDisabledImage(v: Node): void;
        
        getOpacity(): number;
        setOpacity(v: number): void;

        getColor(): Color3B;
        setColor(v: Color3B): void;

        static create(normalImage: Node, selectedImage: Node, disabledImage: Node, selector?: (sender: Node) => void , target?: Node): MenuItemSprite;
    }
    export class MenuItemToggle extends MenuItem {
        getOpacity(): number;
        setOpacity(v: number): void;
        getColor(): Color3B;
        setColor(v: Color3B): void;
        getSelectedIndex(): number;
        setSelectedIndex(v: number): void;
        static create(target: Node, selector: (sender: Node) => void , on: MenuItem, off: MenuItem): MenuItemToggle;
    }
    export class ProgressTimer extends NodeRGBA {
        getType(): number;
        setType(v: number): void;

        getPercentage(): number;
        setPercentage(v: number): void;

        setReverseProgress(v: number): void;

        getBarChangeRate(): Point;
        setBarChangeRate(v: Point): void;

        setMidpoint(): Point;
        setMidpoint(v: Point): void;

        isReverseDirection(): boolean;
        setReverseDirection(v: boolean): void;

        static create(sprite:Sprite): ProgressTimer;
    }
    export class Loader {
        static preload(resources:any[], selector?:any, target?:any): Loader;
    }
    export class FileUtils {
        //jsp only
        isFileExist(f: string): boolean;
        getFileDataFromZip(pszZipFilePath: string, fileName: string, size: number): string;
        static sharedFileUtils(): FileUtils;

        //common
        fullPathForFilename(name: string): string;
        fullPathFromRelativeFile(name: string, relativeFile: string): string;
        setSearchResolutionsOrder(searchResolutionsOrder: string): void;
        getSearchResolutionsOrder(): string;

        setSearchPaths(path: string): void;
        addSearchPath(path: string): void;
        isAbsolutePath(path: string): boolean;
        static getInstance(): FileUtils;
    }
    export class AudioEngine {
        static getInstance(): AudioEngine;

        preloadMusic(path: string): void;
        preloadEffect(path: string): void;

        unloadEffect(path: string): void;

        playMusic(path: string, loop: boolean): void;
        stopMusic(releaseData?: boolean): void;

        playEffect(path: string, loop: boolean): void;

        pauseMusic(): void;
        resumeMusic(): void;
        isMusicPlaying(): boolean;

        stopAllEffects(): void;
        pauseAllEffects(): void;

        resumeAllEffects(): void;

        stopEffect(audioID: number): void;
        pauseEffect(audioID: number): void;
        resumeEffect(audioID: number): void;

        getMusicVolume(): number;
        setMusicVolume(v: number): void;

        getEffectsVolume(): number;
        setEffectsVolume(v: number): void;
    }
    export class Scale9Sprite extends NodeRGBA {
        static create(file: string, rect?: Rect, capInsets?: Rect): Scale9Sprite;
        static createWithSpriteFrame(spriteFrame: SpriteFrame, capInsets?: Rect): Scale9Sprite;
        static createWithSpriteFrameName(spriteFrameName:string, capInsets?: Rect): Scale9Sprite;
    }
    export class Control extends Layer {
        isEnabled(): boolean;
        setEnabled(v: boolean): void;

        setHighlighted(v: boolean): void;
        isHighlighted(): boolean;
        
        getOpacity(): number;
        setOpacity(v: number): void;

        getColor(): Color3B;
        setColor(v: Color3B): void;

        getState(): number;

        setSelected(v: boolean): void;
        getTouchLocation(touch: Touch): Point;
        isTouchInside(touch: Touch): boolean;
    }
    export class ControlButton extends Control {
        getAdjustBackgroundImage(): boolean;
        setAdjustBackgroundImage(v: boolean): void;

        getZoomOnTouchDown(): boolean;
        setZoomOnTouchDown(v: boolean): void;

        getPreferredSize(): Size;
        setPreferredSize(v: Size): void;

        getLabelAnchorPoint(): Point;
        setLabelAnchorPoint(v: Point): void;

        getIsPushed(): boolean;

        getVerticalMargin(): number;
        setMargins(marginH: number, marginV: number): void;

        getTitleForState(s: number): string;
        setTitleForState(title: string, state: number): void;

        getTitleColorForState(s: number): Color3B;
        setTitleColorForState(color: Color3B, s: number): void;

        getBackgroundSpriteForState(s: number): Scale9Sprite;
        setBackgroundSpriteForState(sprite: Scale9Sprite, state: number): void;

        static create(label:LabelTTF, backgroundSprite:Scale9Sprite): ControlButton;
    }
    export interface EditBoxDelegate {
        editBoxEditingDidBegin(sender: EditBox): void;
        editBoxEditingDidEnd(sender: EditBox): void;
        editBoxTextChanged(sender: EditBox, text:string): void;
        editBoxReturn(sender: EditBox): void;
    }
    export class EditBox extends ControlButton {
        setFont(fontName: string, fontSize: number): void;
        setText(text: string): void;
        getText(): string;

        setFontColor(color: Color3B): void;

        setDelegate(d: EditBoxDelegate): void;
        getDelegate(): EditBoxDelegate;

        getMaxLength(): number;
        setMaxLength(v: number): void;

        setPlaceHolder(t: string): void;
        setPlaceholderFontColor(color: Color3B): void;

        setInputFlag(f: number): void;
        setReturnType(v: number): void;

        getEffectsVolume(): number;
        setEffectsVolume(v: number): void;

        static create(...args:any[]): EditBox;
        //static create(size: Rect, normal9SpriteBg: Scale9Sprite, press9SpriteBg?: Scale9Sprite, disabled9SpriteBg?: Scale9Sprite): EditBox;
    }
    export interface ScrollViewDelegate{
        scrollViewDidScroll(v: Node): void;
        scrollViewDidZoom(v: Node): void;
    }
    export class ScrollView extends Layer {
        minContainerOffset(): Point;
        maxContainerOffset(): Point;

        getContentOffset(): Point;
        setContentOffset(offset: Point, animated?: boolean): void;
        setContentOffsetInDuration(offset: Point, dt: number): void;

        setZoomScale(scale: number, animated?: boolean): void;
        getZoomScale(): number;
        setZoomScaleInDuration(s: number, dt: number): void;

        isDragging(): boolean;
        isClippingToBounds(): boolean;

        getViewSize(): Size;
        setViewSize(s: Size): void;

        getContainer(): Node;
        setContainer(v: Node): void;

        getDirection(): number;
        setDirection(v: number): void;

        setTouchEnabled(v: boolean): void;

        isBounceable(): boolean;
        setBounceable(v: boolean): void;

        static create(...args: any[]): ScrollView;
    }
    export interface TableViewDelegate {
        scrollViewDidScroll(v: Node): void;
        scrollViewDidZoom(v: Node): void;
        tableCellTouched(t: TableView, cell: TableViewCell): void;
    }
    export interface TableViewDataSource {
        cellSizeForTable(t: TableView): Size;
        tableCellAtIndex(t: TableView, idx: number): TableViewCell;
        numberOfCellsInTableView(t: TableView): number;
    }
    export class TableViewCell extends Node {
        getObjectID(): number;
        setObjectID(v: number): void;

        getIdx(): number;
        getIdx(v: number): void;

        reset(): void;

        static extend(prop: any): TableViewCell;
    }
    export class TableView extends ScrollView {
        reloadData(): void;

        getDataSource(): TableViewDataSource;
        setDataSource(v: TableViewDataSource): void;

        getDelegate(): TableViewDelegate;
        setDelegate(v: TableViewDelegate, isDirectCall?: boolean): void;

        getVerticalFillOrder(): number;
        setVerticalFillOrder(v: number): void;

        updateCellAtIndex(idx: number): void;
        insertCellAtIndex(idx: number): void;
        removeCellAtIndex(idx: number): void;

        dequeueCell(): TableViewCell;
        cellAtIndex(idx: number): TableViewCell;

        static create(dataSource: TableViewDataSource, size: Size): TableView;
    }
    export class ParticleSystem extends Node {        
        setAutoRemoveOnFinish(t: boolean): void;
        isAutoRemoveOnFinish(): boolean;

        getParticleCount(): number;
        setParticleCount(v: number): void;

        getDuration(): number;
        setDuration(v: number): void;

        getSourcePosition(): Point;
        setSourcePosition(v: Point): void;

        getPosVar(): Point;
        setPosVar(v: Point): void;

        getLife(): number;
        setLife(v: number): void;

        getLifeVar(): number;
        setLifeVar(v: number): void;

        getAngle(): number;
        setAngle(v: number): void;

        getAngleVar(): number;
        setAngleVar(v: number): void;

        getGravity(): number;
        setGravity(v: number): void;

        getSpeed(): number;
        setSpeed(v: number): void;

        getSpeedVar(): number;
        setSpeedVar(v: number): void;

        getEmissionRate(): number;
        setEmissionRate(v: number): void;

        getTotalParticles(): number;
        setTotalParticles(v: number): void;

        getTexture(): Texture2D;
        setTexture(v: Texture2D): void;

        static create(pListFile: string): ParticleSystem;
    }

    export class Browser {
        static supportWebGL: boolean;
    }
}
