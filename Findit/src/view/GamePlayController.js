///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />
/// <reference path="../CommonUtils.ts" />
///<reference path="ViewController.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var view;
(function (view) {
    var GameConfig = (function () {
        function GameConfig() {
        }
        GameConfig.createLevel = function () {
            var packageIndex = Math.floor(Math.random() * GameConfig.kPackages.length);
            var packageId = GameConfig.kPackages[packageIndex];
            var images = [];
            for (var i = 1; i <= GameConfig.kMaxImage; i++) {
                images[i - 1] = "res/" + packageId + i + ".jpg";
            }

            var i = images.length;
            var j = 0;
            var temp = null;
            while (--i) {
                j = Math.floor(Math.random() * (i - 1));
                temp = images[i];
                images[i] = images[j];
                images[j] = temp;
            }
            return images;
        };
        GameConfig.kTotalTime = 9;
        GameConfig.kMaxImage = 8;
        GameConfig.kPackages = ["c", "f", "i", "p"];
        GameConfig.kTargetSize = 80;
        GameConfig.kLevelMax = 2;
        GameConfig.kTotalLevels = 3;
        return GameConfig;
    })();
    view.GameConfig = GameConfig;

    var UserinterfaceController = (function (_super) {
        __extends(UserinterfaceController, _super);
        function UserinterfaceController() {
            _super.call(this);
            this.onTimeOut = new js.Signal();
            this.onReplay = new js.Signal();
        }
        UserinterfaceController.prototype.dispose = function () {
            this.onTimeOut.dispose();
            this.onReplay.dispose();
            delete this.onTimeOut;
            delete this.onReplay;
            _super.prototype.dispose.call(this);
        };

        UserinterfaceController.prototype.buildUI = function () {
            var _this = this;
            var layer = new this.Layer(this);
            var size = cc.Director.getInstance().getWinSize();

            var padding = 5;
            var targetBg = cc.Sprite.createWithSpriteFrameName("target_bg");
            var targetSize = targetBg.getContentSize();
            targetBg.setAnchorPoint(cc.p(0, 0));
            targetBg.setPosition(padding, size.height - targetSize.height - padding);
            layer.addChild(targetBg);
            this.targetBg = targetBg;

            var replayButton = new view.Button("replay", "replay");
            var replayButtonLayer = replayButton.buildUI();
            var buttonSize = replayButtonLayer.getContentSize();
            var y = size.height - buttonSize.height - padding;
            var x = size.width - buttonSize.width - padding;
            replayButtonLayer.setPosition(x, y);

            replayButton.selectNode.setOpacity(100);
            replayButton.touchInsideSignal.add(function () {
                _this.onReplay.dispatch();
                cc.AudioEngine.getInstance().playEffect("res/sfx/Button.mp3", false);
            });
            layer.addChild(replayButtonLayer);

            this.replayButton = replayButton;

            var timeBg = cc.Sprite.createWithSpriteFrameName("time_left");
            var timeSize = timeBg.getContentSize();
            var timeX = padding + targetSize.width;
            var timeY = size.height - timeSize.height - padding;
            timeBg.setAnchorPoint(cc.p(0, 0));
            timeBg.setPosition(timeX, timeY);
            layer.addChild(timeBg);
            this.timeBg = timeBg;

            var timeLabel = cc.LabelBMFont.create("9", "res/Numbers.fnt");
            timeLabel.setAnchorPoint(cc.p(0.5, 0));
            timeLabel.setPosition(timeX + timeSize.width / 2, timeY + 15);
            layer.addChild(timeLabel);
            this.timeLabel = timeLabel;

            var scoreBg = cc.Sprite.createWithSpriteFrameName("total_score");
            var scoreSize = scoreBg.getContentSize();
            var scoreWidth = x - timeX - timeSize.width;
            var scoreScale = scoreWidth / scoreSize.width;
            var scoreX = timeX + timeSize.width - 1;
            scoreBg.setAnchorPoint(cc.p(0, 0));
            scoreBg.setScaleX(scoreScale);
            scoreBg.setPosition(scoreX, timeY);
            layer.addChild(scoreBg);
            this.scoreBg = scoreBg;

            var scoreLabel = cc.LabelBMFont.create("953214", "res/Numbers2.fnt");
            scoreLabel.setAnchorPoint(cc.p(0.5, 0));
            scoreLabel.setPosition(scoreX + scoreWidth / 2, timeY + 15);
            layer.addChild(scoreLabel);
            this.scoreLabel = scoreLabel;
            this.scoreLabelPosition = scoreLabel.getPosition();

            var gameover = cc.Sprite.createWithSpriteFrameName("your_score");
            gameover.setPosition(size.width / 2, size.height / 2 - 200);
            gameover.setVisible(false);
            layer.addChild(gameover);
            this.gameover = gameover;

            return layer;
        };

        UserinterfaceController.prototype.start = function () {
            this.startCountDown();
            this.scoreLabel.setString("0");

            if (this.gameover.isVisible()) {
                this.gameover.setVisible(false);

                this.scoreLabel.stopAllActions();
                this.scoreLabel.runAction(cc.MoveTo.create(0.3, cc.p(this.scoreLabelPosition.x, this.scoreLabelPosition.y)));
            }
        };
        UserinterfaceController.prototype.startCountDown = function () {
            var currentTime = GameConfig.kTotalTime;
            var timeLabel = this.timeLabel;

            timeLabel.stopAllActions();
            timeLabel.setString(currentTime + "");

            var context = this;
            var callf = cc.CallFunc.create(function (t, v) {
                currentTime--;
                if (timeLabel && currentTime >= 0) {
                    timeLabel.setString(currentTime + "");
                }
                if (currentTime < 0 && context) {
                    context.onTimeOut.dispatch();
                }
            });
            var seq = cc.Sequence.create(cc.DelayTime.create(1), callf);
            timeLabel.runAction(cc.Repeat.create(seq, GameConfig.kTotalTime + 1));
        };

        UserinterfaceController.prototype.stopCoundDown = function () {
            this.timeLabel.setString("0");
            this.timeLabel.stopAllActions();
            this.updateTargetSprite(null, false);
        };

        UserinterfaceController.prototype.updateTargetSprite = function (targetSprite, succeed) {
            if (this.targetSprite) {
                if (succeed) {
                    var dropTime = 0.35;
                    var flyTime = 0.5;
                    var scorePos = this.scoreLabel.getPosition();
                    var removeFunc = cc.CallFunc.create(function (t, v) {
                        if (t)
                            t.removeFromParent(true);
                    }, this.targetSprite);
                    var hide = cc.Spawn.create([cc.ScaleTo.create(dropTime, 0.35, 0.35), cc.FadeTo.create(dropTime, 100), cc.MoveBy.create(dropTime, cc.p(0, -100))]);
                    var fly = cc.Spawn.create([cc.FadeOut.create(flyTime), cc.MoveTo.create(flyTime, cc.p(scorePos.x + 80, scorePos.y + 50)), cc.RotateBy.create(flyTime, 200), cc.ScaleTo.create(flyTime, 0.01, 0.01)]);
                    this.targetSprite.runAction(cc.Sequence.create([hide, fly, removeFunc]));
                } else {
                    this.targetSprite.removeFromParent(true);
                }
            }
            this.targetSprite = targetSprite;
            if (this.targetSprite) {
                var position = this.targetBg.getPosition();
                var size = this.targetBg.getContentSize();
                this.targetSprite.setAnchorPoint(cc.p(0.5, 0.5));
                this.targetSprite.setOpacity(0);
                this.targetSprite.setScale(1.4);
                this.targetSprite.setPosition(position.x + size.width / 2, position.y + size.height / 2);
                this.targetSprite.runAction(cc.Spawn.create(cc.FadeIn.create(0.3), cc.ScaleTo.create(0.3, 1, 1)));
                this.layer.addChild(this.targetSprite);
            }
        };

        UserinterfaceController.prototype.playGameOver = function () {
            var size = cc.Director.getInstance().getWinSize();

            if (cc.Browser.supportWebGL) {
                var particle = cc.ParticleSystem.create("res/starline.plist");
                particle.setAutoRemoveOnFinish(true);
                particle.setPosition(size.width / 2, size.height / 2 - 40);
                this.layer.addChild(particle);
            }

            this.gameover.setVisible(true);
            this.gameover.setOpacity(0);
            this.gameover.setPosition(size.width / 2, size.height / 2 - 40);
            this.gameover.runAction(cc.FadeIn.create(0.5));

            var move = cc.MoveTo.create(1, cc.p(size.width / 2, size.height / 2));
            this.scoreLabel.stopAllActions();
            this.scoreLabel.runAction(cc.EaseBounceOut.create(move));
        };
        return UserinterfaceController;
    })(view.LayerController);
    view.UserinterfaceController = UserinterfaceController;
    var ContentController = (function (_super) {
        __extends(ContentController, _super);
        function ContentController() {
            _super.call(this);
            this.onGameOver = new js.Signal();
            this.onTouchCorrect = new js.Signal();
            this.onTouchWrong = new js.Signal();
            this.isPlaying = false;
        }
        ContentController.prototype.dispose = function () {
            this.onGameOver.dispose();
            this.onTouchCorrect.dispose();
            this.onTouchWrong.dispose();

            delete this.onGameOver;
            delete this.onTouchCorrect;
            delete this.onTouchWrong;
            delete this.images;

            _super.prototype.dispose.call(this);
        };
        ContentController.prototype.buildUI = function () {
            var layer = new this.Layer(this);
            layer.setTouchEnabled(true);
            layer.setTouchMode(cc.TOUCH_ONE_BY_ONE);

            var size = cc.Director.getInstance().getWinSize();
            layer.setContentSize(cc.size(size.width, size.height - 144));
            return layer;
        };

        ContentController.prototype.start = function (images) {
            this.images = images;
            this.currentIndex = 0;
            this.isPlaying = true;
            this.setCurrentImage(this.images[this.currentIndex]);

            var sfx = "res/sfx/PacificRim.mp3";
            var world = ["res/sfx/World_1.mp3", "res/sfx/World_2.mp3", "res/sfx/World_3.mp3"];
            if (images[0].indexOf("res/p") == -1) {
                sfx = world[Math.floor(Math.random() * 3)];
            }
            cc.AudioEngine.getInstance().stopMusic();
            cc.AudioEngine.getInstance().playMusic(sfx, true);
            cc.AudioEngine.getInstance().setMusicVolume(0.3);
        };

        ContentController.prototype.next = function () {
            this.currentIndex++;
            var levels = Math.min(GameConfig.kTotalLevels - 1, this.images.length);
            if (this.currentIndex >= levels) {
                this.currentIndex = 0;
                this.onGameOver.dispatch();
                this.isPlaying = false;
                this.setCurrentImage(null);
            } else {
                this.isPlaying = true;
                this.setCurrentImage(this.images[this.currentIndex]);
            }
        };
        ContentController.prototype.setCurrentImage = function (file) {
            if (this.currentSprite)
                this.currentSprite.removeFromParent(true);

            var layer = this.layer;
            if (layer) {
                var size = this.layer.getContentSize();
                var sprite = cc.Sprite.create(file);
                var spriteSize = sprite.getContentSize();
                var scale = size.width / spriteSize.width;
                var width = spriteSize.width * scale;
                var height = spriteSize.height * scale;

                sprite.setAnchorPoint(cc.p(0, 0));
                sprite.setScale(scale);
                sprite.setPosition((size.width - width) / 2, (size.height - height) / 2);
                layer.addChild(sprite);
                this.currentSprite = sprite;
            }
        };

        ContentController.prototype.generateTargetArea = function () {
            var sprite = this.currentSprite;
            var spriteSize = sprite.getContentSize();
            var halfSize = GameConfig.kTargetSize / 2;

            var width = spriteSize.width - GameConfig.kTargetSize;
            var height = spriteSize.height - GameConfig.kTargetSize;
            var px = Math.random() * width + halfSize;
            var py = Math.random() * height + halfSize;

            this.tx = px;
            this.ty = py;
            return this.getSpriteUnderPoint(px, py);
        };

        ContentController.prototype.getSpriteUnderTouch = function (touch) {
            var localLocation = this.currentSprite.convertTouchToNodeSpace(touch);
            return this.getSpriteUnderPoint(localLocation.x, localLocation.y);
        };
        ContentController.prototype.getSpriteUnderPoint = function (x, y) {
            if (this.currentSprite) {
                var spriteSize = this.currentSprite.getContentSize();
                var scale = this.currentSprite.getScale();
                var width = spriteSize.width;
                var height = spriteSize.height;

                var texture = this.currentSprite.getTexture();
                var targetEdge = GameConfig.kTargetSize;

                var fx = x - targetEdge / 2;
                if (fx < 0)
                    fx = 0;
                if (fx > width)
                    fx = width;

                var fy = height - y - targetEdge / 2;
                if (fy < 0)
                    fy = 0;
                if (fy > height)
                    fy = height;

                var frame = cc.SpriteFrame.createWithTexture(texture, cc.rect(fx, fy, targetEdge, targetEdge), false);
                var sprite = cc.Sprite.createWithSpriteFrame(frame);

                //sprite.setScale(scale);
                return sprite;
            }
            return null;
        };

        ContentController.prototype.verifyTouch = function (touch) {
            var localLocation = this.currentSprite.convertTouchToNodeSpace(touch);
            var targetEdge = GameConfig.kTargetSize + 25;
            var rect = cc.rect(this.tx - targetEdge / 2, this.ty - targetEdge / 2, targetEdge, targetEdge);
            return cc.rectContainsPoint(rect, localLocation);
        };
        ContentController.prototype.isTouchInside = function (touch) {
            var touchLocation = touch.getLocation();
            touchLocation = this.layer.getParent().convertToNodeSpace(touchLocation);
            var bounding = this.layer.getBoundingBox();
            return cc.rectContainsPoint(bounding, touchLocation);
        };

        ContentController.prototype.onTouchBegan = function (touch, event) {
            _super.prototype.onTouchBegan.call(this, touch, event);
            if (!this.isTouchInside(touch) || !this.isPlaying) {
                return false;
            }
            return true;
        };
        ContentController.prototype.onTouchEnded = function (touch, event) {
            _super.prototype.onTouchEnded.call(this, touch, event);
            if (this.isPlaying) {
                if (this.verifyTouch(touch)) {
                    this.onTouchCorrect.dispatch(this, touch);
                } else {
                    this.onTouchWrong.dispatch(this, touch);
                }
            }
        };
        return ContentController;
    })(view.LayerController);
    view.ContentController = ContentController;
    var EffectsController = (function (_super) {
        __extends(EffectsController, _super);
        function EffectsController() {
            _super.apply(this, arguments);
        }
        EffectsController.prototype.buildUI = function () {
            var layer = new this.Layer(this);
            return layer;
        };
        EffectsController.prototype.playScoreChangeEffect = function (touch, score, plusScore, sprite) {
            var local = touch.getLocation();

            if (sprite) {
                sprite.setPosition(local.x, local.y);
                sprite.setOpacity(150);
                var removeTime = 0.5;
                var removeFunc = cc.CallFunc.create(function (t, v) {
                    if (t)
                        t.removeFromParent(true);
                }, sprite);
                sprite.runAction(cc.Sequence.create(cc.Spawn.create(cc.FadeOut.create(removeTime), cc.ScaleTo.create(removeTime, 1.4, 1.4)), removeFunc));
                this.layer.addChild(sprite);
            }

            var font = "res/Numbers3.fnt";
            if (score > 0) {
                var particle = cc.ParticleSystem.create("res/correct.plist");
                particle.setAutoRemoveOnFinish(true);
                particle.setPosition(local.x, local.y);
                this.layer.addChild(particle);
            } else
                font = "res/Numbers.fnt";
            var scoreStr = "" + score;

            if (plusScore > 0)
                scoreStr += "+" + plusScore;
            var label = cc.LabelBMFont.create(scoreStr, font);
            label.setPosition(local.x, local.y);
            label.setOpacity(180);
            label.setScale(0.6);

            var animationTime = 0.6;
            var removeFunc = cc.CallFunc.create(function (t, v) {
                if (t)
                    t.removeFromParent(true);
            }, label);
            var moveOut = cc.EaseBackOut.create(cc.MoveBy.create(animationTime, cc.p(0, 38)));
            label.runAction(cc.Sequence.create([cc.Spawn.create(cc.FadeTo.create(animationTime, 100), moveOut), cc.FadeOut.create(0.3), removeFunc]));
            this.layer.addChild(label);
        };
        return EffectsController;
    })(view.LayerController);
    view.EffectsController = EffectsController;
    var GamePlayController = (function (_super) {
        __extends(GamePlayController, _super);
        function GamePlayController() {
            _super.apply(this, arguments);
            this.score = 0;
        }
        GamePlayController.prototype.onInit = function (reference) {
            _super.prototype.onInit.call(this, reference);
            var context = this;

            var size = cc.Director.getInstance().getWinSize();
            var background = cc.Sprite.create("res/tiled.png");
            var backSize = background.getContentSize();
            background.setScaleX(size.width / backSize.width);
            background.setScaleY(size.height / backSize.height);
            background.setPosition(size.width / 2, size.height / 2);
            this.scene.addChild(background);

            this.content = new ContentController();
            this.scene.addChild(this.content.buildUI());

            this.ui = new UserinterfaceController();
            this.scene.addChild(this.ui.buildUI());

            this.effects = new EffectsController();
            this.scene.addChild(this.effects.buildUI());

            this.content.onTouchCorrect.add(function (sender, touch) {
                if (context)
                    context.onTouchCorrect(touch);
            });
            this.content.onTouchWrong.add(function (sender, touch) {
                if (context)
                    context.onTouchWrong(touch);
            });
            this.content.onGameOver.add(function () {
                if (context)
                    context.onGameOver();
            });
            this.ui.onTimeOut.add(function () {
                if (context)
                    context.onCountDownTimeout(false);
            });
            this.ui.onReplay.add(function () {
                if (context)
                    context.startGame();
            });

            this.startGame();
        };

        GamePlayController.prototype.onCountDownTimeout = function (succeed) {
            this.targetUseTimes++;

            if (this.targetUseTimes > GameConfig.kLevelMax) {
                this.targetUseTimes = 0;
                this.content.next();
                cc.AudioEngine.getInstance().playEffect("res/sfx/LevelPass.mp3", false);
            }
            this.ui.startCountDown();
            this.changeTargetSprite(succeed);
        };

        GamePlayController.prototype.startGame = function () {
            this.score = 0;
            this.targetUseTimes = 0;
            this.wrongTouchTimes = 0;
            this.correctTouchTimes = 0;
            this.content.start(GameConfig.createLevel());
            this.ui.start();
            this.changeTargetSprite(false);
        };

        GamePlayController.prototype.changeTargetSprite = function (succeed) {
            this.ui.updateTargetSprite(this.content.generateTargetArea(), succeed);
        };

        GamePlayController.prototype.onTouchCorrect = function (touch) {
            this.wrongTouchTimes = 0;
            this.correctTouchTimes++;

            var weightTimes = (this.correctTouchTimes - 1);
            if (weightTimes < 0)
                weightTimes = 0;
            if (weightTimes > 7)
                weightTimes = 7;

            var plusScore = weightTimes * 10;

            var addScore = 100;
            this.score += addScore + plusScore;

            this.ui.scoreLabel.setString(this.score + "");
            this.onCountDownTimeout(true);
            this.effects.playScoreChangeEffect(touch, addScore, plusScore, this.content.getSpriteUnderTouch(touch));

            cc.AudioEngine.getInstance().playEffect("res/sfx/Player_Hit.mp3", false);
            if (this.correctTouchTimes > 1) {
                var sfx = this.correctTouchTimes - 1;
                if (sfx > 6)
                    sfx = 6;
                cc.AudioEngine.getInstance().playEffect("res/sfx/note" + sfx + ".mp3", false);
            }
        };
        GamePlayController.prototype.onTouchWrong = function (touch) {
            this.wrongTouchTimes++;
            if (this.correctTouchTimes > 0) {
                cc.AudioEngine.getInstance().playEffect("res/sfx/ComboEnd.mp3", false);
            }
            this.correctTouchTimes = 0;

            var weightTimes = this.wrongTouchTimes - 2;
            if (weightTimes < 0)
                weightTimes = 0;
            var wrongScore = Math.floor(weightTimes * 2);
            if (wrongScore > 0 && this.score > 0) {
                this.effects.playScoreChangeEffect(touch, -wrongScore, 0, null);
            }

            this.score -= wrongScore;
            if (this.score < 0)
                this.score = 0;
            this.ui.scoreLabel.setString(this.score + "");

            cc.AudioEngine.getInstance().playEffect("res/sfx/NoHit.mp3", false);
        };
        GamePlayController.prototype.onGameOver = function () {
            cc.AudioEngine.getInstance().playEffect("res/sfx/GameStart.mp3", false);
            this.ui.stopCoundDown();
            this.ui.playGameOver();
        };
        GamePlayController.create = function () {
            var wrapper = new GamePlayController();
            return new wrapper.Scene(wrapper);
        };
        return GamePlayController;
    })(view.SceneController);
    view.GamePlayController = GamePlayController;
})(view || (view = {}));
//# sourceMappingURL=GamePlayController.js.map
