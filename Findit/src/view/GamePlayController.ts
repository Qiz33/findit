///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />
/// <reference path="../CommonUtils.ts" />
///<reference path="ViewController.ts"/>

module view {
    export class GameConfig {
        public static kTotalTime: number = 9;
        public static kMaxImage: number = 8;
        public static kPackages: string[] = ["c", "f", "i", "p"];
        public static kTargetSize: number = 80;
        public static kLevelMax: number = 2;
        public static kTotalLevels: number = 3;

        public static createLevel(): string[] {
            var packageIndex: number = Math.floor(Math.random() * GameConfig.kPackages.length);
            var packageId: string = GameConfig.kPackages[packageIndex];
            var images: string[] = [];
            for (var i: number = 1; i <= GameConfig.kMaxImage; i++) {
                images[i - 1] = "res/" + packageId + i + ".jpg";
            }

            var i: number = images.length;
            var j: number = 0;
            var temp:string = null;
            while (--i) {
                j = Math.floor(Math.random() * (i - 1));
                temp = images[i];
                images[i] = images[j];
                images[j] = temp;
            }
            return images;
        }
    }

    export class UserinterfaceController extends LayerController {
        public replayButton: view.Button;

        public targetBg: cc.Sprite;
        public timeBg: cc.Sprite;
        public scoreBg: cc.Sprite;
        public gameover: cc.Sprite;

        public timeLabel: cc.LabelBMFont;
        public scoreLabel: cc.LabelBMFont;

        public onTimeOut: js.Signal;
        public onReplay: js.Signal;

        private scoreLabelPosition: cc.Point;
        private targetSprite: cc.Sprite;

        constructor() {
            super();
            this.onTimeOut = new js.Signal();
            this.onReplay = new js.Signal();
        }

        public dispose(): void {
            this.onTimeOut.dispose();
            this.onReplay.dispose();
            delete this.onTimeOut;
            delete this.onReplay;
            super.dispose();
        }

        public buildUI(): cc.Layer {
            var layer: cc.Layer = new this.Layer(this);
            var size: cc.Size = cc.Director.getInstance().getWinSize();   

            var padding: number = 5;
            var targetBg: cc.Sprite = cc.Sprite.createWithSpriteFrameName("target_bg");
            var targetSize: cc.Size = targetBg.getContentSize();
            targetBg.setAnchorPoint(cc.p(0, 0));
            targetBg.setPosition(padding, size.height - targetSize.height - padding);
            layer.addChild(targetBg);
            this.targetBg = targetBg;

            var replayButton: view.Button = new view.Button("replay", "replay");
            var replayButtonLayer: cc.Layer = replayButton.buildUI();
            var buttonSize: cc.Size = replayButtonLayer.getContentSize();
            var y: number = size.height - buttonSize.height - padding;
            var x: number = size.width - buttonSize.width - padding;
            replayButtonLayer.setPosition(x, y);

            replayButton.selectNode.setOpacity(100);
            replayButton.touchInsideSignal.add(() => {
                this.onReplay.dispatch();
                cc.AudioEngine.getInstance().playEffect("res/sfx/Button.mp3", false);
            });
            layer.addChild(replayButtonLayer);
            
            this.replayButton = replayButton;

            var timeBg: cc.Sprite = cc.Sprite.createWithSpriteFrameName("time_left");
            var timeSize: cc.Size = timeBg.getContentSize();
            var timeX: number = padding + targetSize.width;
            var timeY: number = size.height - timeSize.height - padding;
            timeBg.setAnchorPoint(cc.p(0, 0));
            timeBg.setPosition(timeX, timeY);
            layer.addChild(timeBg);
            this.timeBg = timeBg;

            var timeLabel: cc.LabelBMFont = cc.LabelBMFont.create("9", "res/Numbers.fnt");
            timeLabel.setAnchorPoint(cc.p(0.5, 0));
            timeLabel.setPosition(timeX + timeSize.width/2, timeY + 15);
            layer.addChild(timeLabel);
            this.timeLabel = timeLabel;

            var scoreBg: cc.Sprite = cc.Sprite.createWithSpriteFrameName("total_score");
            var scoreSize: cc.Size = scoreBg.getContentSize();
            var scoreWidth: number = x - timeX - timeSize.width;
            var scoreScale: number = scoreWidth / scoreSize.width;
            var scoreX: number = timeX + timeSize.width - 1;
            scoreBg.setAnchorPoint(cc.p(0, 0));
            scoreBg.setScaleX(scoreScale)
            scoreBg.setPosition(scoreX, timeY);
            layer.addChild(scoreBg);
            this.scoreBg = scoreBg;

            var scoreLabel: cc.LabelBMFont = cc.LabelBMFont.create("953214", "res/Numbers2.fnt");
            scoreLabel.setAnchorPoint(cc.p(0.5, 0));
            scoreLabel.setPosition(scoreX + scoreWidth/2, timeY + 15);
            layer.addChild(scoreLabel);
            this.scoreLabel = scoreLabel;
            this.scoreLabelPosition = scoreLabel.getPosition();

            var gameover: cc.Sprite = cc.Sprite.createWithSpriteFrameName("your_score");
            gameover.setPosition(size.width / 2, size.height / 2 - 200);
            gameover.setVisible(false);
            layer.addChild(gameover);
            this.gameover = gameover;
            
            return layer;
        }

        public start(): void {
            this.startCountDown();
            this.scoreLabel.setString("0");

            if (this.gameover.isVisible()) {
                this.gameover.setVisible(false);

                this.scoreLabel.stopAllActions();
                this.scoreLabel.runAction(cc.MoveTo.create(0.3, cc.p(this.scoreLabelPosition.x, this.scoreLabelPosition.y)));
            }
        }
        public startCountDown(): void {
            var currentTime: number = GameConfig.kTotalTime;
            var timeLabel: cc.LabelBMFont = this.timeLabel;

            timeLabel.stopAllActions();
            timeLabel.setString(currentTime + "");

            var context: UserinterfaceController = this;
            var callf: cc.CallFunc = cc.CallFunc.create((t?: cc.Node, v?: any) => {
                currentTime--;
                if (timeLabel && currentTime >= 0) {
                    timeLabel.setString(currentTime + "");
                }
                if (currentTime < 0 && context) {
                    context.onTimeOut.dispatch();
                }
            });
            var seq: cc.Sequence = cc.Sequence.create(cc.DelayTime.create(1), callf);
            timeLabel.runAction(cc.Repeat.create(seq, GameConfig.kTotalTime + 1));
        }

        public stopCoundDown() {
            this.timeLabel.setString("0");
            this.timeLabel.stopAllActions();
            this.updateTargetSprite(null, false);
        }

        public updateTargetSprite(targetSprite:cc.Sprite, succeed:boolean): void {
            if (this.targetSprite) {
                if (succeed) {
                    var dropTime: number = 0.35;
                    var flyTime: number = 0.5;
                    var scorePos: cc.Point = this.scoreLabel.getPosition();
                    var removeFunc: cc.CallFunc = cc.CallFunc.create((t?: cc.Node, v?: any) => { if (t) t.removeFromParent(true) }, this.targetSprite);
                    var hide: cc.Spawn = cc.Spawn.create([cc.ScaleTo.create(dropTime, 0.35, 0.35), cc.FadeTo.create(dropTime, 100), cc.MoveBy.create(dropTime, cc.p(0, -100))]);
                    var fly: cc.Spawn = cc.Spawn.create([cc.FadeOut.create(flyTime), cc.MoveTo.create(flyTime, cc.p(scorePos.x + 80, scorePos.y + 50)), cc.RotateBy.create(flyTime, 200), cc.ScaleTo.create(flyTime, 0.01, 0.01)]);
                    this.targetSprite.runAction(cc.Sequence.create([hide, fly, removeFunc]));
                } else {
                    this.targetSprite.removeFromParent(true);
                }
            }
            this.targetSprite = targetSprite;
            if (this.targetSprite) {
                var position: cc.Point = this.targetBg.getPosition();
                var size: cc.Size = this.targetBg.getContentSize();
                this.targetSprite.setAnchorPoint(cc.p(0.5, 0.5));
                this.targetSprite.setOpacity(0);
                this.targetSprite.setScale(1.4);
                this.targetSprite.setPosition(position.x + size.width / 2, position.y + size.height / 2);
                this.targetSprite.runAction(cc.Spawn.create(cc.FadeIn.create(0.3), cc.ScaleTo.create(0.3, 1, 1)));
                this.layer.addChild(this.targetSprite);                
            }
        }

        public playGameOver(): void {
            var size: cc.Size = cc.Director.getInstance().getWinSize();   

            if (cc.Browser.supportWebGL) {
                var particle: cc.ParticleSystem = cc.ParticleSystem.create("res/starline.plist");
                particle.setAutoRemoveOnFinish(true);
                particle.setPosition(size.width / 2, size.height / 2 - 40);
                this.layer.addChild(particle);
            }

            this.gameover.setVisible(true);
            this.gameover.setOpacity(0);            
            this.gameover.setPosition(size.width / 2, size.height / 2 - 40);
            this.gameover.runAction(cc.FadeIn.create(0.5));

            var move: cc.MoveTo = cc.MoveTo.create(1, cc.p(size.width / 2, size.height / 2));
            this.scoreLabel.stopAllActions();
            this.scoreLabel.runAction(cc.EaseBounceOut.create(move));
        }
    }
    export class ContentController extends LayerController {
        public onTouchCorrect: js.Signal;
        public onTouchWrong: js.Signal;
        public onGameOver: js.Signal;

        private currentSprite: cc.Sprite;
        private images: string[];
        private currentIndex: number;

        private tx: number;
        private ty: number;

        private isPlaying: boolean;

        constructor() {
            super();
            this.onGameOver = new js.Signal();
            this.onTouchCorrect = new js.Signal();
            this.onTouchWrong = new js.Signal();
            this.isPlaying = false;
        }

        public dispose(): void {
            this.onGameOver.dispose();
            this.onTouchCorrect.dispose();
            this.onTouchWrong.dispose();

            delete this.onGameOver;
            delete this.onTouchCorrect;
            delete this.onTouchWrong;
            delete this.images;

            super.dispose();
        }
        public buildUI(): cc.Layer {
            var layer: cc.Layer = new this.Layer(this);
            layer.setTouchEnabled(true);
            layer.setTouchMode(cc.TOUCH_ONE_BY_ONE);

            var size: cc.Size = cc.Director.getInstance().getWinSize();
            layer.setContentSize(cc.size(size.width, size.height - 144));
            return layer;
        }

        public start(images: string[]): void {
            this.images = images;
            this.currentIndex = 0;
            this.isPlaying = true;
            this.setCurrentImage(this.images[this.currentIndex]);

            var sfx: string = "res/sfx/PacificRim.mp3";
            var world: string[] = ["res/sfx/World_1.mp3", "res/sfx/World_2.mp3", "res/sfx/World_3.mp3"];
            if (images[0].indexOf("res/p") == -1) {
                sfx = world[Math.floor(Math.random() * 3)];
            }
            cc.AudioEngine.getInstance().stopMusic();
            cc.AudioEngine.getInstance().playMusic(sfx, true);
            cc.AudioEngine.getInstance().setMusicVolume(0.3);
        }

        public next(): void {
            this.currentIndex++;
            var levels: number = Math.min(GameConfig.kTotalLevels-1, this.images.length);
            if (this.currentIndex >= levels) {
                this.currentIndex = 0;
                this.onGameOver.dispatch();
                this.isPlaying = false;
                this.setCurrentImage(null);
            } else {
                this.isPlaying = true;
                this.setCurrentImage(this.images[this.currentIndex]);
            }
            
        }
        public setCurrentImage(file: string): void {
            if (this.currentSprite) this.currentSprite.removeFromParent(true);

            var layer: cc.Layer = this.layer;
            if (layer) {
                var size: cc.Size = this.layer.getContentSize();
                var sprite: cc.Sprite = cc.Sprite.create(file);
                var spriteSize: cc.Size = sprite.getContentSize();
                var scale: number = size.width / spriteSize.width;
                var width: number = spriteSize.width * scale;
                var height: number = spriteSize.height * scale;

                sprite.setAnchorPoint(cc.p(0, 0));
                sprite.setScale(scale);
                sprite.setPosition((size.width - width) / 2, (size.height - height) / 2);
                layer.addChild(sprite);
                this.currentSprite = sprite;
            }
        }

        public generateTargetArea(): cc.Sprite {
            var sprite: cc.Sprite = this.currentSprite;
            var spriteSize: cc.Size = sprite.getContentSize();
            var halfSize: number = GameConfig.kTargetSize / 2;

            var width: number = spriteSize.width - GameConfig.kTargetSize;
            var height: number = spriteSize.height - GameConfig.kTargetSize;
            var px: number = Math.random() * width + halfSize;
            var py: number = Math.random() * height + halfSize;

            this.tx = px;
            this.ty = py;
            return this.getSpriteUnderPoint(px, py);
        }

        public getSpriteUnderTouch(touch: cc.Touch): cc.Sprite {
            var localLocation: cc.Point = this.currentSprite.convertTouchToNodeSpace(touch);//touch.getLocation();//
            return this.getSpriteUnderPoint(localLocation.x, localLocation.y);
        }
        public getSpriteUnderPoint(x: number, y: number): cc.Sprite {
            
            if (this.currentSprite) {
                var spriteSize: cc.Size = this.currentSprite.getContentSize();
                var scale: number = this.currentSprite.getScale();
                var width: number = spriteSize.width;
                var height: number = spriteSize.height;

                var texture: cc.Texture2D = this.currentSprite.getTexture();
                var targetEdge: number = GameConfig.kTargetSize;

                var fx: number = x - targetEdge / 2;
                if (fx < 0) fx = 0;
                if (fx > width) fx = width;

                var fy: number = height - y - targetEdge / 2;
                if (fy < 0) fy = 0;
                if (fy > height) fy = height;

                var frame: cc.SpriteFrame = cc.SpriteFrame.createWithTexture(texture, cc.rect(fx, fy, targetEdge, targetEdge), false);
                var sprite: cc.Sprite = cc.Sprite.createWithSpriteFrame(frame);
                //sprite.setScale(scale);
                return sprite;
            }
            return null;
        }

        public verifyTouch(touch: cc.Touch): boolean {
            var localLocation: cc.Point = this.currentSprite.convertTouchToNodeSpace(touch);
            var targetEdge: number = GameConfig.kTargetSize + 25;
            var rect: cc.Rect = cc.rect(this.tx - targetEdge / 2, this.ty - targetEdge / 2, targetEdge, targetEdge);
            return cc.rectContainsPoint(rect, localLocation);
        }
        public isTouchInside(touch: cc.Touch): boolean {
            var touchLocation: cc.Point = touch.getLocation();
            touchLocation = this.layer.getParent().convertToNodeSpace(touchLocation);
            var bounding: cc.Rect = this.layer.getBoundingBox();
            return cc.rectContainsPoint(bounding, touchLocation);
        }

        public onTouchBegan(touch: cc.Touch, event: any): boolean {
            super.onTouchBegan(touch, event);
            if (!this.isTouchInside(touch) || !this.isPlaying) {
                return false;
            }
            return true;
        }
        public onTouchEnded(touch: cc.Touch, event: any): void {
            super.onTouchEnded(touch, event);
            if (this.isPlaying) {
                if (this.verifyTouch(touch)) {
                    this.onTouchCorrect.dispatch(this, touch);
                } else {
                    this.onTouchWrong.dispatch(this, touch);
                }
            }
        }
    }
    export class EffectsController extends LayerController {
        public buildUI(): cc.Layer {
            var layer: cc.Layer = new this.Layer(this);
            return layer;
        }
        public playScoreChangeEffect(touch: cc.Touch, score: number, plusScore:number, sprite:cc.Sprite): void {
            var local: cc.Point = touch.getLocation();

            if (sprite) {
                sprite.setPosition(local.x, local.y);
                sprite.setOpacity(150);
                var removeTime: number = 0.5;
                var removeFunc: cc.CallFunc = cc.CallFunc.create((t?: cc.Node, v?: any) => { if (t) t.removeFromParent(true) }, sprite);
                sprite.runAction(cc.Sequence.create(cc.Spawn.create(cc.FadeOut.create(removeTime), cc.ScaleTo.create(removeTime, 1.4, 1.4)), removeFunc));
                this.layer.addChild(sprite);
            }

            var font: string = "res/Numbers3.fnt";
            if (score > 0) {
                var particle: cc.ParticleSystem = cc.ParticleSystem.create("res/correct.plist");
                particle.setAutoRemoveOnFinish(true);
                particle.setPosition(local.x, local.y);
                this.layer.addChild(particle);
            } else font = "res/Numbers.fnt";
            var scoreStr: string = "" + score;
            
            if (plusScore > 0) scoreStr += "+" + plusScore;      
            var label: cc.LabelBMFont = cc.LabelBMFont.create(scoreStr, font);            
            label.setPosition(local.x, local.y);
            label.setOpacity(180);
            label.setScale(0.6);

            var animationTime: number = 0.6;
            var removeFunc: cc.CallFunc = cc.CallFunc.create((t?: cc.Node, v?: any) => { if (t) t.removeFromParent(true) }, label);
            var moveOut: cc.EaseBackOut = cc.EaseBackOut.create(cc.MoveBy.create(animationTime, cc.p(0, 38)));
            label.runAction(cc.Sequence.create([cc.Spawn.create(cc.FadeTo.create(animationTime, 100), moveOut),cc.FadeOut.create(0.3) ,removeFunc]));
            this.layer.addChild(label);
        }

    }
    export class GamePlayController extends SceneController {
        private ui: UserinterfaceController;
        private content: ContentController;
        private effects: EffectsController;

        private score: number = 0;
        private targetUseTimes: number;

        private wrongTouchTimes: number;
        private correctTouchTimes: number;

        public onInit(reference: any): void {
            super.onInit(reference);
            var context: GamePlayController = this;

            var size: cc.Size = cc.Director.getInstance().getWinSize();
            var background: cc.Sprite = cc.Sprite.create("res/tiled.png");
            var backSize: cc.Size = background.getContentSize();
            background.setScaleX(size.width / backSize.width);
            background.setScaleY(size.height / backSize.height);
            background.setPosition(size.width / 2, size.height / 2);
            this.scene.addChild(background);

            this.content = new ContentController();            
            this.scene.addChild(this.content.buildUI());

            this.ui = new UserinterfaceController();            
            this.scene.addChild(this.ui.buildUI());

            this.effects = new EffectsController();
            this.scene.addChild(this.effects.buildUI());
      
            this.content.onTouchCorrect.add((sender: LayerController, touch: cc.Touch) => {
                if (context) context.onTouchCorrect(touch);
            });
            this.content.onTouchWrong.add((sender: LayerController, touch: cc.Touch) => {
                if (context) context.onTouchWrong(touch);                
            });
            this.content.onGameOver.add(() => {
                if (context) context.onGameOver();
            });
            this.ui.onTimeOut.add(() => {
                if (context) context.onCountDownTimeout(false);
            });
            this.ui.onReplay.add(() => {
                if (context) context.startGame();
            });

            this.startGame();
        }

        public onCountDownTimeout(succeed:boolean): void {
            this.targetUseTimes++;

            if (this.targetUseTimes > GameConfig.kLevelMax) {
                this.targetUseTimes = 0;
                this.content.next();
                cc.AudioEngine.getInstance().playEffect("res/sfx/LevelPass.mp3", false);
            } 
            this.ui.startCountDown();
            this.changeTargetSprite(succeed);
        }

        public startGame(): void {
            this.score = 0;
            this.targetUseTimes = 0;
            this.wrongTouchTimes = 0;
            this.correctTouchTimes = 0;
            this.content.start(GameConfig.createLevel());
            this.ui.start();
            this.changeTargetSprite(false);
        }

        private changeTargetSprite(succeed:boolean): void {
            this.ui.updateTargetSprite(this.content.generateTargetArea(), succeed);
        }

        private onTouchCorrect(touch: cc.Touch): void {
            this.wrongTouchTimes = 0;
            this.correctTouchTimes++;

            var weightTimes: number = (this.correctTouchTimes - 1);
            if (weightTimes < 0) weightTimes = 0;
            if (weightTimes > 7) weightTimes = 7;

            var plusScore: number = weightTimes * 10;

            var addScore: number = 100;
            this.score += addScore + plusScore;
            
            this.ui.scoreLabel.setString(this.score + "");
            this.onCountDownTimeout(true);    
            this.effects.playScoreChangeEffect(touch, addScore, plusScore, this.content.getSpriteUnderTouch(touch));  
            
            cc.AudioEngine.getInstance().playEffect("res/sfx/Player_Hit.mp3", false); 
            if (this.correctTouchTimes > 1) {
                var sfx: number = this.correctTouchTimes - 1;
                if (sfx > 6) sfx = 6;
                cc.AudioEngine.getInstance().playEffect("res/sfx/note"+sfx + ".mp3", false);
            }
        }
        private onTouchWrong(touch: cc.Touch): void {
            this.wrongTouchTimes++;
            if (this.correctTouchTimes > 0) {
                cc.AudioEngine.getInstance().playEffect("res/sfx/ComboEnd.mp3", false);
            }
            this.correctTouchTimes = 0;

            var weightTimes: number = this.wrongTouchTimes - 2;
            if (weightTimes < 0) weightTimes = 0;
            var wrongScore = Math.floor(weightTimes * 2);
            if (wrongScore > 0 && this.score > 0) {
                this.effects.playScoreChangeEffect(touch, -wrongScore, 0, null);
            }

            this.score -= wrongScore;
            if (this.score < 0) this.score = 0;
            this.ui.scoreLabel.setString(this.score + "");

            cc.AudioEngine.getInstance().playEffect("res/sfx/NoHit.mp3", false);
        }
        private onGameOver(): void {
            cc.AudioEngine.getInstance().playEffect("res/sfx/GameStart.mp3", false);
            this.ui.stopCoundDown();
            this.ui.playGameOver();
        }
        public static create(): cc.Scene {
            var wrapper: GamePlayController = new GamePlayController();
            return new wrapper.Scene(wrapper);
        }
    }
}