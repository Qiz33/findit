///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />
///<reference path="ViewController.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var view;
(function (view) {
    var Button = (function (_super) {
        __extends(Button, _super);
        function Button(normalName, selectName) {
            if (normalName) {
                this.normalNode = cc.Sprite.createWithSpriteFrameName(normalName);
                this.normalNode.setAnchorPoint(cc.p(0, 0));
            }
            if (selectName) {
                this.selectNode = cc.Sprite.createWithSpriteFrameName(selectName);
                this.selectNode.setAnchorPoint(cc.p(0, 0));
            }
            this.currentNode = this.normalNode;
            this.touchInsideSignal = new js.Signal();
            this.touchOutsideSignal = new js.Signal();

            _super.call(this);
        }
        Button.prototype.dispose = function () {
            this.touchInsideSignal.dispose();
            this.touchOutsideSignal.dispose();

            delete this.touchInsideSignal;
            delete this.touchOutsideSignal;

            delete this.normalNode;
            delete this.selectNode;

            _super.prototype.dispose.call(this);
        };

        Button.prototype.buildUI = function () {
            var layer = new this.Layer(this);
            layer.setTouchEnabled(true);
            layer.setTouchMode(cc.TOUCH_ONE_BY_ONE);

            layer.addChild(this.normalNode);
            layer.addChild(this.selectNode);

            this.selectNode.setVisible(false);

            var size = this.normalNode.getContentSize();
            layer.setContentSize(cc.size(size.width, size.height));

            this.layer = layer;
            return layer;
        };

        Button.prototype.isTouchInside = function (touch) {
            var touchLocation = touch.getLocation();
            touchLocation = this.layer.getParent().convertToNodeSpace(touchLocation);
            var bounding = this.layer.getBoundingBox();
            return cc.rectContainsPoint(bounding, touchLocation);
        };

        Button.prototype.onTouchBegan = function (touch, event) {
            _super.prototype.onTouchBegan.call(this, touch, event);
            if (!this.isTouchInside(touch)) {
                return false;
            }
            this.selectNode.setVisible(true);
            this.normalNode.setVisible(false);

            return true;
        };
        Button.prototype.onTouchEnded = function (touch, event) {
            _super.prototype.onTouchEnded.call(this, touch, event);
            this.selectNode.setVisible(false);
            this.normalNode.setVisible(true);

            var touchInsided = this.isTouchInside(touch);
            if (touchInsided) {
                this.touchInsideSignal.dispatch(this);
            } else {
                this.touchOutsideSignal.dispatch(this);
            }
        };

        Button.prototype.onTouchCancelled = function (touch, event) {
            this.selectNode.setVisible(false);
            this.normalNode.setVisible(true);
            _super.prototype.onTouchCancelled.call(this, touch, event);
        };
        return Button;
    })(view.LayerController);
    view.Button = Button;
})(view || (view = {}));
//# sourceMappingURL=Button.js.map
