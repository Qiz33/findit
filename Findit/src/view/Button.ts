///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />
///<reference path="ViewController.ts"/>

module view {
    export class Button extends LayerController {
        public touchInsideSignal: js.Signal;
        public touchOutsideSignal: js.Signal;

        public normalNode: cc.Sprite;
        public selectNode: cc.Sprite;

        public currentNode: cc.Sprite;

        public label: cc.LabelTTF;

        constructor(normalName: string, selectName: string) {
            if (normalName) {
                this.normalNode = cc.Sprite.createWithSpriteFrameName(normalName);
                this.normalNode.setAnchorPoint(cc.p(0, 0));
            }
            if (selectName) {
                this.selectNode = cc.Sprite.createWithSpriteFrameName(selectName);
                this.selectNode.setAnchorPoint(cc.p(0, 0));
            }
            this.currentNode = this.normalNode;
            this.touchInsideSignal = new js.Signal();
            this.touchOutsideSignal = new js.Signal();

            super();
        }

        public dispose(): void {
            this.touchInsideSignal.dispose();
            this.touchOutsideSignal.dispose();

            delete this.touchInsideSignal;
            delete this.touchOutsideSignal;

            delete this.normalNode;
            delete this.selectNode;

            super.dispose();
        }

        public buildUI(): cc.Layer {
            var layer: cc.Layer = new this.Layer(this);
            layer.setTouchEnabled(true);
            layer.setTouchMode(cc.TOUCH_ONE_BY_ONE);

            layer.addChild(this.normalNode);
            layer.addChild(this.selectNode);

            this.selectNode.setVisible(false);

            var size: cc.Size = this.normalNode.getContentSize();
            layer.setContentSize(cc.size(size.width, size.height));
            
            this.layer = layer;
            return layer;
        }

        public isTouchInside(touch: cc.Touch): boolean {
            var touchLocation: cc.Point = touch.getLocation();
            touchLocation = this.layer.getParent().convertToNodeSpace(touchLocation);
            var bounding: cc.Rect = this.layer.getBoundingBox();
            return cc.rectContainsPoint(bounding, touchLocation);
        }

        public onTouchBegan(touch: cc.Touch, event: any): boolean {
            super.onTouchBegan(touch, event);
            if (!this.isTouchInside(touch)) {
                return false;
            }
            this.selectNode.setVisible(true);
            this.normalNode.setVisible(false);

            return true;
        }
        public onTouchEnded(touch: cc.Touch, event: any): void {
            super.onTouchEnded(touch, event);
            this.selectNode.setVisible(false);
            this.normalNode.setVisible(true);

            var touchInsided = this.isTouchInside(touch);
            if (touchInsided) {
                this.touchInsideSignal.dispatch(this);
            } else {
                this.touchOutsideSignal.dispatch(this);
            }
        }

        public onTouchCancelled(touch: cc.Touch, event: any): void {
            this.selectNode.setVisible(false);
            this.normalNode.setVisible(true);
            super.onTouchCancelled(touch, event);
        }

    }

}