///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var view;
(function (view) {
    var CocosController = (function () {
        function CocosController() {
            this.initialize();
        }
        CocosController.prototype.initialize = function () {
            this.initSignal = new js.Signal();
            this.cleanupSignal = new js.Signal();
            this.enterSignal = new js.Signal();
            this.exitSignal = new js.Signal();
            this.enterTransitionDidFinishSignal = new js.Signal();
            this.exitTransitionDidStartSignal = new js.Signal();
        };

        CocosController.prototype.dispose = function () {
            this.isDisposed = true;

            this.initSignal.dispose();
            this.enterSignal.dispose();
            this.exitSignal.dispose();
            this.enterTransitionDidFinishSignal.dispose();
            this.exitTransitionDidStartSignal.dispose();
            this.cleanupSignal.dispose();

            delete this.initSignal;
            delete this.enterSignal;
            delete this.exitSignal;
            delete this.enterTransitionDidFinishSignal;
            delete this.exitTransitionDidStartSignal;
            delete this.cleanupSignal;

            delete this.reference;
            //console.log("disposed controller");
        };

        CocosController.prototype.onInit = function (reference) {
            this.reference = reference;
            this.initSignal.dispatch();
        };

        CocosController.prototype.onCleanup = function () {
            this.cleanupSignal.dispatch();

            this.dispose();
        };

        CocosController.prototype.onEnter = function () {
            this.enterSignal.dispatch();
        };

        CocosController.prototype.onExit = function () {
            this.exitSignal.dispatch();
        };

        CocosController.prototype.onEnterTransitionDidFinish = function () {
            this.enterTransitionDidFinishSignal.dispatch();
        };

        CocosController.prototype.onExitTransitionDidStart = function () {
            this.exitTransitionDidStartSignal.dispatch();
        };
        return CocosController;
    })();
    view.CocosController = CocosController;

    var SceneController = (function (_super) {
        __extends(SceneController, _super);
        function SceneController() {
            _super.apply(this, arguments);
        }
        SceneController.prototype.initialize = function () {
            _super.prototype.initialize.call(this);

            //var context: SceneController = this;
            this.Scene = cc.Scene.extend({
                ctor: function (controller) {
                    this._super();
                    this.context = controller;
                    this.context.onInit(this);
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                }
            });
        };
        SceneController.prototype.dispose = function () {
            delete this.Scene;
            delete this.scene;
            _super.prototype.dispose.call(this);
        };
        SceneController.prototype.onInit = function (reference) {
            _super.prototype.onInit.call(this, reference);
            this.scene = reference;
        };
        SceneController.prototype.onCleanup = function () {
            delete this.scene;
            _super.prototype.onCleanup.call(this);
        };

        SceneController.create = function () {
            var wrapper = new SceneController();
            return new wrapper.Scene(wrapper);
        };
        return SceneController;
    })(CocosController);
    view.SceneController = SceneController;

    var BaseLayerController = (function (_super) {
        __extends(BaseLayerController, _super);
        function BaseLayerController() {
            _super.apply(this, arguments);
        }
        BaseLayerController.prototype.initialize = function () {
            _super.prototype.initialize.call(this);

            this.touchBeganSignal = new js.Signal();
            this.touchMovedSignal = new js.Signal();
            this.touchEndedSignal = new js.Signal();
            this.touchCancelledSignal = new js.Signal();

            this.touchesBeganSignal = new js.Signal();
            this.touchesMovedSignal = new js.Signal();
            this.touchesEndedSignal = new js.Signal();
            this.touchesCancelledSignal = new js.Signal();
        };
        BaseLayerController.prototype.dispose = function () {
            this.touchBeganSignal.dispose();
            this.touchMovedSignal.dispose();
            this.touchEndedSignal.dispose();
            this.touchCancelledSignal.dispose();

            this.touchesBeganSignal.dispose();
            this.touchesMovedSignal.dispose();
            this.touchesEndedSignal.dispose();
            this.touchesCancelledSignal.dispose();

            delete this.touchBeganSignal;
            delete this.touchMovedSignal;
            delete this.touchEndedSignal;
            delete this.touchCancelledSignal;

            delete this.touchesBeganSignal;
            delete this.touchesMovedSignal;
            delete this.touchesEndedSignal;
            delete this.touchesCancelledSignal;

            _super.prototype.dispose.call(this);
        };

        //touch
        BaseLayerController.prototype.onTouchBegan = function (touch, event) {
            this.touchBeganSignal.dispatch();
            return true;
        };
        BaseLayerController.prototype.onTouchMoved = function (touch, event) {
            this.touchMovedSignal.dispatch();
        };
        BaseLayerController.prototype.onTouchEnded = function (touch, event) {
            this.touchEndedSignal.dispatch();
        };
        BaseLayerController.prototype.onTouchCancelled = function (touch, event) {
            this.touchCancelledSignal.dispatch();
        };

        //touches
        BaseLayerController.prototype.onTouchesBegan = function (touch, event) {
            this.touchesBeganSignal.dispatch();
        };
        BaseLayerController.prototype.onTouchesMoved = function (touch, event) {
            this.touchesMovedSignal.dispatch();
        };
        BaseLayerController.prototype.onTouchesEnded = function (touch, event) {
            this.touchesEndedSignal.dispatch();
        };
        BaseLayerController.prototype.onTouchesCancelled = function (touch, event) {
            this.touchesCancelledSignal.dispatch();
        };
        return BaseLayerController;
    })(CocosController);
    view.BaseLayerController = BaseLayerController;

    var LayerController = (function (_super) {
        __extends(LayerController, _super);
        function LayerController() {
            _super.apply(this, arguments);
        }
        LayerController.prototype.initialize = function () {
            _super.prototype.initialize.call(this);

            //var context: LayerController = this;
            this.Layer = cc.Layer.extend({
                ctor: function (context) {
                    this._super();
                    this.context = context;
                    context.onInit(this);
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                },
                onTouchBegan: function (touch, event) {
                    return this.context.onTouchBegan(touch, event);
                },
                onTouchMoved: function (touch, event) {
                    this.context.onTouchMoved(touch, event);
                },
                onTouchEnded: function (touch, event) {
                    this.context.onTouchEnded(touch, event);
                },
                onTouchCancelled: function (touch, event) {
                    this.context.onTouchCancelled(touch, event);
                },
                onTouchesBegan: function (touch, event) {
                    this.context.onTouchesBegan(touch, event);
                },
                onTouchesMoved: function (touch, event) {
                    this.context.onTouchesMoved(touch, event);
                },
                onTouchesEnded: function (touch, event) {
                    this.context.onTouchesEnded(touch, event);
                },
                onTouchesCancelled: function (touch, event) {
                    this.context.onTouchesCancelled(touch, event);
                }
            });
        };
        LayerController.prototype.dispose = function () {
            delete this.Layer;
            delete this.layer;
            _super.prototype.dispose.call(this);
        };
        LayerController.prototype.onInit = function (reference) {
            _super.prototype.onInit.call(this, reference);
            this.layer = reference;
        };
        LayerController.prototype.onCleanup = function () {
            delete this.layer;
            _super.prototype.onCleanup.call(this);
        };

        LayerController.create = function () {
            var wrapper = new LayerController();
            return new wrapper.Layer(wrapper);
        };
        return LayerController;
    })(BaseLayerController);
    view.LayerController = LayerController;

    var LayerColorController = (function (_super) {
        __extends(LayerColorController, _super);
        function LayerColorController() {
            _super.apply(this, arguments);
        }
        LayerColorController.prototype.dispose = function () {
            delete this.Layer;
            delete this.layer;
            _super.prototype.dispose.call(this);
        };
        LayerColorController.prototype.initialize = function () {
            _super.prototype.initialize.call(this);

            //var context: LayerColorController = this;
            this.Layer = cc.LayerColor.extend({
                ctor: function (context) {
                    this._super();
                    this.context = context;
                    context.onInit(this);
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                },
                onTouchBegan: function (touch, event) {
                    return this.context.onTouchBegan(touch, event);
                },
                onTouchMoved: function (touch, event) {
                    this.context.onTouchMoved(touch, event);
                },
                onTouchEnded: function (touch, event) {
                    this.context.onTouchEnded(touch, event);
                },
                onTouchCancelled: function (touch, event) {
                    this.context.onTouchCancelled(touch, event);
                },
                onTouchesBegan: function (touch, event) {
                    this.context.onTouchesBegan(touch, event);
                },
                onTouchesMoved: function (touch, event) {
                    this.context.onTouchesMoved(touch, event);
                },
                onTouchesEnded: function (touch, event) {
                    this.context.onTouchesEnded(touch, event);
                },
                onTouchesCancelled: function (touch, event) {
                    this.context.onTouchesCancelled(touch, event);
                }
            });
        };

        LayerColorController.prototype.onInit = function (reference) {
            _super.prototype.onInit.call(this, reference);
            this.layer = reference;
        };
        LayerColorController.prototype.onCleanup = function () {
            delete this.layer;
            _super.prototype.onCleanup.call(this);
        };

        LayerColorController.create = function () {
            var wrapper = new LayerColorController();
            return new wrapper.Layer(wrapper);
        };
        return LayerColorController;
    })(BaseLayerController);
    view.LayerColorController = LayerColorController;

    var TableViewController = (function (_super) {
        __extends(TableViewController, _super);
        function TableViewController() {
            _super.apply(this, arguments);
        }
        TableViewController.prototype.dispose = function () {
            delete this.Layer;
            delete this.layer;
            _super.prototype.dispose.call(this);
        };
        TableViewController.prototype.initialize = function () {
            _super.prototype.initialize.call(this);

            //var context: TableViewController = this;
            this.Layer = cc.Layer.extend({
                ctor: function (context) {
                    this._super();
                    context.onInit(this);
                    this.context = context;
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                },
                scrollViewDidScroll: function (view) {
                    this.context.scrollViewDidScroll(view);
                },
                scrollViewDidZoom: function (view) {
                    this.context.scrollViewDidZoom(view);
                },
                tableCellTouched: function (table, cell) {
                    this.context.tableCellTouched(table, cell);
                },
                cellSizeForTable: function (table) {
                    return this.context.cellSizeForTable(table);
                },
                tableCellAtIndex: function (table, idx) {
                    return this.context.tableCellAtIndex(table, idx);
                },
                numberOfCellsInTableView: function (table) {
                    return this.context.numberOfCellsInTableView(table);
                }
            });
        };

        TableViewController.prototype.onInit = function (reference) {
            _super.prototype.onInit.call(this, reference);
            this.layer = reference;
        };
        TableViewController.prototype.onCleanup = function () {
            delete this.Layer;
            delete this.layer;
            _super.prototype.onCleanup.call(this);
        };

        TableViewController.prototype.scrollViewDidScroll = function (view) {
        };
        TableViewController.prototype.scrollViewDidZoom = function (view) {
        };
        TableViewController.prototype.tableCellTouched = function (table, cell) {
        };
        TableViewController.prototype.cellSizeForTable = function (table) {
            return cc.size(60, 60);
        };
        TableViewController.prototype.tableCellAtIndex = function (table, idx) {
            return null;
        };
        TableViewController.prototype.numberOfCellsInTableView = function (table) {
            return 0;
        };
        return TableViewController;
    })(BaseLayerController);
    view.TableViewController = TableViewController;
})(view || (view = {}));
//# sourceMappingURL=ViewController.js.map
