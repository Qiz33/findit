///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />
/// <reference path="../CommonUtils.ts" />
///<reference path="ViewController.ts"/>
///<reference path="GamePlayController.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var view;
(function (view) {
    var StartupController = (function (_super) {
        __extends(StartupController, _super);
        function StartupController() {
            _super.apply(this, arguments);
        }
        StartupController.prototype.onInit = function (reference) {
            _super.prototype.onInit.call(this, reference);

            var frameCache = cc.SpriteFrameCache.getInstance();
            frameCache.addSpriteFrames("res/ui.plist", "res/ui.png");

            var size = cc.Director.getInstance().getWinSize();

            var background = cc.Sprite.create("res/background.png");
            background.setAnchorPoint(cc.p(0.5, 0));
            var backSize = background.getContentSize();
            var scale = size.height / backSize.height;
            background.setScale(scale);
            background.setPositionX(size.width / 2);
            this.scene.addChild(background);

            var startButton = new view.Button("start_play", "start_play");
            var buttonLayer = startButton.buildUI();
            var buttonSize = buttonLayer.getContentSize();
            var y = size.height * 0.3;
            var x = size.width - buttonSize.width - 80;
            buttonLayer.setPosition(x, y);

            var gameTitle = cc.Sprite.createWithSpriteFrameName("game_title");
            var gameTitleSize = gameTitle.getContentSize();
            gameTitle.setAnchorPoint(cc.p(0, 0));
            gameTitle.setPosition(x - gameTitleSize.width - 5, y);
            this.scene.addChild(gameTitle);

            startButton.selectNode.setOpacity(100);
            startButton.touchInsideSignal.add(function () {
                cc.AudioEngine.getInstance().playEffect("res/sfx/Button.mp3", false);
                cc.Director.getInstance().replaceScene(view.GamePlayController.create());
            });
            this.scene.addChild(buttonLayer);

            this.startButton = startButton;
        };

        StartupController.prototype.onEnter = function () {
            _super.prototype.onEnter.call(this);

            cc.AudioEngine.getInstance().playMusic("res/sfx/Menu.mp3", true);
            cc.AudioEngine.getInstance().setMusicVolume(0.4);
        };

        StartupController.prototype.onExit = function () {
            _super.prototype.onExit.call(this);
        };

        StartupController.create = function () {
            var wrapper = new StartupController();
            return new wrapper.Scene(wrapper);
        };
        return StartupController;
    })(view.SceneController);
    view.StartupController = StartupController;
})(view || (view = {}));
//# sourceMappingURL=StartupController.js.map
