///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />
/// <reference path="../CommonUtils.ts" />
///<reference path="ViewController.ts"/>
///<reference path="GamePlayController.ts"/>

module view {
    export class StartupController extends SceneController {
        private startButton: view.Button;

        public onInit(reference: any): void {
            super.onInit(reference);

            var frameCache: cc.SpriteFrameCache = cc.SpriteFrameCache.getInstance();
            frameCache.addSpriteFrames("res/ui.plist", "res/ui.png");

            var size: cc.Size = cc.Director.getInstance().getWinSize();

            var background: cc.Sprite = cc.Sprite.create("res/background.png");
            background.setAnchorPoint(cc.p(0.5, 0));
            var backSize: cc.Size = background.getContentSize();
            var scale: number = size.height/backSize.height;
            background.setScale(scale);
            background.setPositionX(size.width/2);
            this.scene.addChild(background);

            var startButton: view.Button = new view.Button("start_play", "start_play");
            var buttonLayer: cc.Layer = startButton.buildUI();
            var buttonSize: cc.Size = buttonLayer.getContentSize();
            var y: number = size.height * 0.3;
            var x: number = size.width - buttonSize.width - 80;
            buttonLayer.setPosition(x, y);

            var gameTitle: cc.Sprite = cc.Sprite.createWithSpriteFrameName("game_title");
            var gameTitleSize: cc.Size = gameTitle.getContentSize();
            gameTitle.setAnchorPoint(cc.p(0, 0));
            gameTitle.setPosition(x - gameTitleSize.width - 5, y);
            this.scene.addChild(gameTitle);

            startButton.selectNode.setOpacity(100);            
            startButton.touchInsideSignal.add(() => {
                cc.AudioEngine.getInstance().playEffect("res/sfx/Button.mp3", false);
                cc.Director.getInstance().replaceScene(GamePlayController.create());
            });
            this.scene.addChild(buttonLayer);

            this.startButton = startButton;
        }


        public onEnter(): void {
            super.onEnter();
            
            cc.AudioEngine.getInstance().playMusic("res/sfx/Menu.mp3", true);
            cc.AudioEngine.getInstance().setMusicVolume(0.4);
        }

        public onExit(): void {
            super.onExit();
        }

        public static create(): cc.Scene {
            var wrapper: StartupController = new StartupController();
            return new wrapper.Scene(wrapper);
        }
    }
}