///<reference path="../../dtd/cocos2dx.d.ts"/>
/// <reference path="../Signal.ts" />

module view {
    export class CocosController {
        public reference: any;

        public initSignal: js.Signal;
        public cleanupSignal: js.Signal;
        public enterSignal: js.Signal;
        public exitSignal: js.Signal;
        public enterTransitionDidFinishSignal: js.Signal;
        public exitTransitionDidStartSignal: js.Signal;

        public isDisposed: boolean;

        constructor() {
            this.initialize();
        }

        public initialize(): void {
            this.initSignal = new js.Signal();
            this.cleanupSignal = new js.Signal();
            this.enterSignal = new js.Signal();
            this.exitSignal = new js.Signal();
            this.enterTransitionDidFinishSignal = new js.Signal();
            this.exitTransitionDidStartSignal = new js.Signal();
        }

        public dispose(): void {
            this.isDisposed = true;

            this.initSignal.dispose();
            this.enterSignal.dispose();
            this.exitSignal.dispose();
            this.enterTransitionDidFinishSignal.dispose();
            this.exitTransitionDidStartSignal.dispose();
            this.cleanupSignal.dispose();

            delete this.initSignal;
            delete this.enterSignal;
            delete this.exitSignal;
            delete this.enterTransitionDidFinishSignal;
            delete this.exitTransitionDidStartSignal;
            delete this.cleanupSignal;

            delete this.reference;
            //console.log("disposed controller");
        }

        public onInit(reference: any): void {
            this.reference = reference;
            this.initSignal.dispatch();
        }

        public onCleanup(): void {
            this.cleanupSignal.dispatch();

            this.dispose();
        }

        public onEnter(): void {
            this.enterSignal.dispatch();
        }

        public onExit(): void {
            this.exitSignal.dispatch();
        }

        public onEnterTransitionDidFinish(): void {
            this.enterTransitionDidFinishSignal.dispatch();
        }

        public onExitTransitionDidStart(): void {
            this.exitTransitionDidStartSignal.dispatch();
        }
    }

    export class SceneController extends CocosController {
        public Scene: any;

        public scene: cc.Scene;

        public initialize(): void {
            super.initialize();
            //var context: SceneController = this;

            this.Scene = cc.Scene.extend({
                ctor: function (controller: SceneController) {
                    this._super();
                    this.context = controller;
                    this.context.onInit(this);
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                }
            });
        }
        public dispose(): void {
            delete this.Scene;
            delete this.scene;
            super.dispose();
        }
        public onInit(reference: any): void {
            super.onInit(reference);
            this.scene = reference;
        }
        public onCleanup(): void {
            delete this.scene;
            super.onCleanup();
        }

        public static create(): cc.Scene {
            var wrapper: SceneController = new SceneController();
            return new wrapper.Scene(wrapper);
        }
    }

    export class BaseLayerController extends CocosController {
        public touchBeganSignal: js.Signal;
        public touchMovedSignal: js.Signal;
        public touchEndedSignal: js.Signal;
        public touchCancelledSignal: js.Signal;

        public touchesBeganSignal: js.Signal;
        public touchesMovedSignal: js.Signal;
        public touchesEndedSignal: js.Signal;
        public touchesCancelledSignal: js.Signal;

        public initialize(): void {
            super.initialize();

            this.touchBeganSignal = new js.Signal();
            this.touchMovedSignal = new js.Signal();
            this.touchEndedSignal = new js.Signal();
            this.touchCancelledSignal = new js.Signal();

            this.touchesBeganSignal = new js.Signal();
            this.touchesMovedSignal = new js.Signal();
            this.touchesEndedSignal = new js.Signal();
            this.touchesCancelledSignal = new js.Signal();
        }
        public dispose(): void {
            this.touchBeganSignal.dispose();
            this.touchMovedSignal.dispose();
            this.touchEndedSignal.dispose();
            this.touchCancelledSignal.dispose();

            this.touchesBeganSignal.dispose();
            this.touchesMovedSignal.dispose();
            this.touchesEndedSignal.dispose();
            this.touchesCancelledSignal.dispose();

            delete this.touchBeganSignal;
            delete this.touchMovedSignal;
            delete this.touchEndedSignal;
            delete this.touchCancelledSignal;

            delete this.touchesBeganSignal;
            delete this.touchesMovedSignal;
            delete this.touchesEndedSignal;
            delete this.touchesCancelledSignal;

            super.dispose();
        }

        //touch
        public onTouchBegan(touch: cc.Touch, event: any): boolean {
            this.touchBeganSignal.dispatch();
            return true;
        }
        public onTouchMoved(touch: cc.Touch, event: any): void {
            this.touchMovedSignal.dispatch();
        }
        public onTouchEnded(touch: cc.Touch, event: any): void {
            this.touchEndedSignal.dispatch();
        }
        public onTouchCancelled(touch: cc.Touch, event: any): void {
            this.touchCancelledSignal.dispatch();
        }
        //touches
        public onTouchesBegan(touch: cc.Touch, event: any): void {
            this.touchesBeganSignal.dispatch();
        }
        public onTouchesMoved(touch: cc.Touch, event: any): void {
            this.touchesMovedSignal.dispatch();
        }
        public onTouchesEnded(touch: cc.Touch, event: any): void {
            this.touchesEndedSignal.dispatch();
        }
        public onTouchesCancelled(touch: cc.Touch, event: any): void {
            this.touchesCancelledSignal.dispatch();
        }

    }

    export class LayerController extends BaseLayerController {
        public Layer: any;

        public layer: cc.Layer;

        public initialize(): void {
            super.initialize();
            //var context: LayerController = this;

            this.Layer = cc.Layer.extend({
                ctor: function (context) {
                    this._super();
                    this.context = context;
                    context.onInit(this);
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                },

                onTouchBegan: function (touch: cc.Touch, event: any): boolean {
                    return this.context.onTouchBegan(touch, event);
                },
                onTouchMoved: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchMoved(touch, event);
                },
                onTouchEnded: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchEnded(touch, event);
                },
                onTouchCancelled: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchCancelled(touch, event);
                },

                onTouchesBegan: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesBegan(touch, event);
                },
                onTouchesMoved: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesMoved(touch, event);
                },
                onTouchesEnded: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesEnded(touch, event);
                },
                onTouchesCancelled: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesCancelled(touch, event);
                }
            });
        }
        public dispose(): void {
            delete this.Layer;
            delete this.layer;
            super.dispose();
        }
        public onInit(reference: any): void {
            super.onInit(reference);
            this.layer = reference;
        }
        public onCleanup(): void {
            delete this.layer;
            super.onCleanup();
        }

        public static create(): cc.Layer {
            var wrapper: LayerController = new LayerController();
            return new wrapper.Layer(wrapper);
        }
    }

    export class LayerColorController extends BaseLayerController {
        public Layer: any;
        public layer: cc.LayerColor;
        public dispose(): void {
            delete this.Layer;
            delete this.layer;
            super.dispose();
        }
        public initialize(): void {
            super.initialize();
            //var context: LayerColorController = this;

            this.Layer = cc.LayerColor.extend({
                ctor: function (context) {
                    this._super();
                    this.context = context;
                    context.onInit(this);
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                },

                onTouchBegan: function (touch: cc.Touch, event: any): boolean {
                    return this.context.onTouchBegan(touch, event);
                },
                onTouchMoved: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchMoved(touch, event);
                },
                onTouchEnded: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchEnded(touch, event);
                },
                onTouchCancelled: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchCancelled(touch, event);
                },

                onTouchesBegan: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesBegan(touch, event);
                },
                onTouchesMoved: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesMoved(touch, event);
                },
                onTouchesEnded: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesEnded(touch, event);
                },
                onTouchesCancelled: function (touch: cc.Touch, event: any): void {
                    this.context.onTouchesCancelled(touch, event);
                }
            });
        }

        public onInit(reference: any): void {
            super.onInit(reference);
            this.layer = reference;
        }
        public onCleanup(): void {
            delete this.layer;
            super.onCleanup();
        }

        public static create(): cc.LayerColor {
            var wrapper: LayerColorController = new LayerColorController();
            return new wrapper.Layer(wrapper);
        }
    }

    export class TableViewController extends BaseLayerController {
        public Layer: any;
        public layer: cc.Layer;

        public dispose(): void {
            delete this.Layer;
            delete this.layer;
            super.dispose();
        }
        public initialize(): void {
            super.initialize();
            //var context: TableViewController = this;

            this.Layer = cc.Layer.extend({
                ctor: function (context) {
                    this._super();
                    context.onInit(this);
                    this.context = context;
                },
                cleanup: function () {
                    this._super();
                    this.context.onCleanup();
                    this.context = null;
                    delete this.context;
                },
                onEnter: function () {
                    this._super();
                    this.context.onEnter();
                },
                onExit: function () {
                    this._super();
                    this.context.onExit();
                },
                onEnterTransitionDidFinish: function () {
                    this._super();
                    this.context.onEnterTransitionDidFinish();
                },
                onExitTransitionDidStart: function () {
                    this._super();
                    this.context.onExitTransitionDidStart();
                },
                scrollViewDidScroll: function (view: cc.TableView) {
                    this.context.scrollViewDidScroll(view);
                },
                scrollViewDidZoom: function (view: cc.TableView) {
                    this.context.scrollViewDidZoom(view);
                },
                tableCellTouched: function (table: cc.TableView, cell: cc.TableViewCell) {
                    this.context.tableCellTouched(table, cell);
                },
                cellSizeForTable: function (table: cc.TableView) {
                    return this.context.cellSizeForTable(table);
                },
                tableCellAtIndex: function (table: cc.TableView, idx: number) {
                    return this.context.tableCellAtIndex(table, idx);
                },
                numberOfCellsInTableView: function (table: cc.TableView) {
                    return this.context.numberOfCellsInTableView(table);
                }

            });
        }

        public onInit(reference: any): void {
            super.onInit(reference);
            this.layer = reference;
        }
        public onCleanup(): void {
            delete this.Layer;
            delete this.layer;
            super.onCleanup();
        }

        public scrollViewDidScroll(view: cc.TableView): void {
        }
        public scrollViewDidZoom(view: cc.TableView): void {
        }
        public tableCellTouched(table: cc.TableView, cell: cc.TableViewCell): void {
        }
        public cellSizeForTable(table: cc.TableView): cc.Size {
            return cc.size(60, 60);
        }
        public tableCellAtIndex(table: cc.TableView, idx: number): cc.TableViewCell {
            return null;
        }
        public numberOfCellsInTableView(table: cc.TableView): number {
            return 0;
        }

    }
}