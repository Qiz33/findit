module res {
    export class ResourceList {
        private _resource: any[];
        private _script: string[];

        constructor() {
            this._resource = [
                //image
                { type: "image", src: "res/background.png" },
                { type: "image", src: "res/tiled.png" },
                { type: "image", src: "res/ui.png" },
                { type: "image", src: "res/Numbers.png" },
                { type: "image", src: "res/Numbers2.png" },
                { type: "image", src: "res/Numbers3.png" },

                { type: "image", src: "res/correct.png" },
                { type: "image", src: "res/fire.png" },
                { type: "image", src: "res/starline.png" },

                { type: "image", src: "res/c1.jpg" },
                { type: "image", src: "res/c2.jpg" },
                { type: "image", src: "res/c3.jpg" },
                { type: "image", src: "res/c4.jpg" },
                { type: "image", src: "res/c5.jpg" },
                { type: "image", src: "res/c6.jpg" },
                { type: "image", src: "res/c7.jpg" },
                { type: "image", src: "res/c8.jpg" },


                { type: "image", src: "res/f1.jpg" },
                { type: "image", src: "res/f2.jpg" },
                { type: "image", src: "res/f3.jpg" },
                { type: "image", src: "res/f4.jpg" },
                { type: "image", src: "res/f5.jpg" },
                { type: "image", src: "res/f6.jpg" },
                { type: "image", src: "res/f7.jpg" },
                { type: "image", src: "res/f8.jpg" },

                { type: "image", src: "res/i1.jpg" },
                { type: "image", src: "res/i2.jpg" },
                { type: "image", src: "res/i3.jpg" },
                { type: "image", src: "res/i4.jpg" },
                { type: "image", src: "res/i5.jpg" },
                { type: "image", src: "res/i6.jpg" },
                { type: "image", src: "res/i7.jpg" },
                { type: "image", src: "res/i8.jpg" },

                { type: "image", src: "res/p1.jpg" },
                { type: "image", src: "res/p2.jpg" },
                { type: "image", src: "res/p3.jpg" },
                { type: "image", src: "res/p4.jpg" },
                { type: "image", src: "res/p5.jpg" },
                { type: "image", src: "res/p6.jpg" },
                { type: "image", src: "res/p7.jpg" },
                { type: "image", src: "res/p8.jpg" },

                //plist
                { type: "plist", src: "res/ui.plist" },
                { type: "plist", src: "res/correct.plist" },
                { type: "plist", src: "res/fire.plist" },
                { type: "plist", src: "res/starline.plist" },

                //fnt
                { type: "fnt", src: "res/Numbers.fnt" },
                { type: "fnt", src: "res/Numbers2.fnt" },
                { type: "fnt", src: "res/Numbers3.fnt" },
                //appcmd set config /section:staticContent /+[fileExtension='.fnt',mimeType='text/plain']

                //effect
                { type: "mp3", src: "res/sfx/Button.mp3" },
                { type: "mp3", src: "res/sfx/ComboEnd.mp3" },
                { type: "mp3", src: "res/sfx/GameStart.mp3" },
                { type: "mp3", src: "res/sfx/LevelPass.mp3" },
                { type: "mp3", src: "res/sfx/Menu.mp3" },
                { type: "mp3", src: "res/sfx/NoHit.mp3" },
                { type: "mp3", src: "res/sfx/note1.mp3" },
                { type: "mp3", src: "res/sfx/note2.mp3" },
                { type: "mp3", src: "res/sfx/note3.mp3" },
                { type: "mp3", src: "res/sfx/note4.mp3" },
                { type: "mp3", src: "res/sfx/note5.mp3" },
                { type: "mp3", src: "res/sfx/note6.mp3" },
                { type: "mp3", src: "res/sfx/PacificRim.mp3" },
                { type: "mp3", src: "res/sfx/Player_Hit.mp3" },
                { type: "mp3", src: "res/sfx/World_1.mp3" },
                { type: "mp3", src: "res/sfx/World_2.mp3" },
                { type: "mp3", src: "res/sfx/World_3.mp3" }
            ];

            //cocos2d's jsloader will call que.push('main.js') to load main.js.
            this._script = [
                "src/Signal.js",
                "src/CommonUtils.js",

                "src/view/ViewController.js",
                "src/view/Button.js",
                "src/view/StartupController.js",
                "src/view/GamePlayController.js"
            ]
        }

        public getResources(): any[] {
            return this._resource;
        }

        public getScript(): string[] {
            return this._script;
        }

        private static _instance: ResourceList;
        public static getInstance(): ResourceList {
            if (ResourceList._instance == null) {
                ResourceList._instance = new ResourceList();
            }
            return ResourceList._instance;
        }
    }
}